module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/css/App.css":
/*!****************************!*\
  !*** ./assets/css/App.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/Destaque.js":
/*!********************************!*\
  !*** ./components/Destaque.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/Destaque.js";



var Destaque = function Destaque() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "box-destaque pt-3 pt-md-5 pb-3 pb-md-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    lg: "6",
    md: "8",
    xs: "12",
    className: "text-white pt-3 pt-md-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "mb-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, "Sistema de Gest\xE3o Online Gratuito", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-block",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-teal",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "Experimente Gr\xE1tis"), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "por 7 dias!"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, "Sistema de gest\xE3o completo para pequenas e m\xE9dias empresas"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, "O VHSYS \xE9 o Sistema de Gest\xE3o Empresarial Online que integra todas as \xE1reas da sua empresa. Com ele voc\xEA t\xEAm:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
    className: "mt-5 mb-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: "7",
    xs: "12",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroupItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, "Emiss\xE3o de notas fiscais eletr\xF4nicas;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroupItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, "Gest\xE3o de vendas e estoque;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroupItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, "Controle financeiro;"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: "5",
    xs: "12",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroup"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroupItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, "Emiss\xE3o de boletos;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroupItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "Or\xE7amento de vendas;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["ListGroupItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, "Integra\xE7\xE3o cont\xE1bil;"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    lg: {
      size: 4,
      offset: 2
    },
    xs: "12",
    md: "4",
    className: "box-form",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Form"], {
    className: "bg-white rounded shadow p-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    for: "exampleEmail",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, "Digite um e-mail"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    type: "email",
    name: "user_email",
    id: "user_email",
    placeholder: "seu@email.com",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    for: "examplePassword",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, "Nome completo"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    type: "text",
    name: "user_name",
    id: "user_name",
    placeholder: "Seu nome..",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    for: "examplePassword",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, "J\xE1 tem uma empresa formalizada?"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    type: "select",
    name: "user_company",
    id: "user_company",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
    value: "0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, "Sim"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("option", {
    value: "1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }, "N\xE3o"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    for: "examplePassword",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  }, "Digite seu nome de usu\xE1rio"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    type: "text",
    name: "user_nick",
    id: "user_nick",
    placeholder: "usuario",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    for: "examplePassword",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }, "Digite uma senha"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    type: "password",
    name: "user_password",
    id: "user_password",
    placeholder: "senha",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    className: "rounded-pill w-100",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: this
  }, "Experimente Gr\xE1tis"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  }, "Clicando no bot\xE3o acima voc\xEA concorda com nossos ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  }, "termos de uso.")))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Destaque);

/***/ }),

/***/ "./components/Layout.js":
/*!******************************!*\
  !*** ./components/Layout.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Menu */ "./components/Menu.js");
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/Layout.js";



var Layout = function Layout(_ref) {
  var children = _ref.children;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Menu__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }), children);
};

/* harmony default export */ __webpack_exports__["default"] = (Layout);

/***/ }),

/***/ "./components/Menu.js":
/*!****************************!*\
  !*** ./components/Menu.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Menu; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime-corejs2/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var svg_inline_react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! svg-inline-react */ "svg-inline-react");
/* harmony import */ var svg_inline_react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(svg_inline_react__WEBPACK_IMPORTED_MODULE_8__);






var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/Menu.js";


 // plugin para inline svg

var Menu =
/*#__PURE__*/
function (_React$Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(Menu, _React$Component);

  function Menu(props) {
    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Menu);

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Menu).call(this, props));
    _this.toggle = _this.toggle.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this));
    _this.state = {
      isOpen: false
    };
    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Menu, [{
    key: "toggle",
    value: function toggle() {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
  }, {
    key: "render",
    value: function render() {
      var svgSource = "<svg width=\"150px\" height=\"40px\" viewBox=\"0 0 413.211 112.19\" class=\"\"><path d=\"m111.65 48.18c-0.024-0.129-0.047-0.266-0.073-0.398l-0.029-0.156c-1.333-8.359-6.613-8.752-8.2-8.672-0.115 8e-3 -0.233 0.014-0.348 0.025-1.763 0.173-3.344 1.016-4.345 2.317-1.149 1.497-1.526 3.397-1.123 5.646 0.646 3.204 1.025 6.318 1.059 8.746 0.103 11.046-4.026 21.537-11.623 29.557-7.574 7.99-17.789 12.694-28.765 13.241-0.58 0.026-1.171 0.044-1.751 0.052-11.329 0.104-22.023-4.207-30.116-12.141-8.099-7.941-12.62-18.562-12.729-29.903-0.103-11.021 4.016-21.519 11.599-29.556 7.566-8.02 17.758-12.735 28.707-13.282 0.576-0.028 1.159-0.043 1.738-0.048 4.311-0.122 8.284 0.92 11.786 1.837 2.879 0.754 5.364 1.408 7.491 1.3 2.47-0.12 4.159-1.312 5.461-3.855 0.701-1.617 0.739-3.234 0.103-4.804-0.774-1.923-2.516-3.599-4.664-4.491-6.48-2.446-13.321-3.658-20.303-3.592-0.769 6e-3 -1.546 0.031-2.311 0.07-14.478 0.723-27.94 6.934-37.915 17.495-9.999 10.596-15.431 24.464-15.293 39.056 0.142 14.993 6.105 29.03 16.789 39.517 10.684 10.492 24.809 16.189 39.782 16.048 0.769-9e-3 1.554-0.03 2.312-0.069 14.505-0.729 27.992-6.938 37.98-17.503 10.017-10.591 15.458-24.463 15.318-39.047-0.023-2.704-0.201-5.117-0.537-7.39\" clip-path=\"url(#a)\" fill=\"#435d95\"></path><path d=\"m111.14 4.483c-0.501-0.941-1.172-1.758-1.996-2.445-0.79-0.649-1.647-1.141-2.573-1.485-0.962-0.36-1.959-0.544-2.967-0.544-2.505 0-4.693 1.082-6.163 3.046l-41.103 55.916-13.018-16.877-0.021-0.024-0.019-0.025c-1.553-1.957-3.746-3.031-6.174-3.031-1.118 0-2.197 0.224-3.215 0.673-0.936 0.408-1.772 0.969-2.495 1.664-0.751 0.727-1.352 1.578-1.781 2.53-0.454 0.995-0.688 2.088-0.688 3.227 0 1.025 0.214 1.994 0.635 2.89 0.289 0.593 0.611 1.131 0.979 1.633l0.041 0.054 0.043 0.052 19.379 24.72c0.83 1.037 1.739 1.815 2.728 2.342 1.113 0.585 2.298 0.874 3.548 0.874 1.297 0 2.539-0.334 3.703-0.985 0.999-0.572 1.881-1.341 2.638-2.29l47.639-63.66c0.513-0.629 0.922-1.323 1.219-2.072 0.34-0.84 0.514-1.772 0.514-2.764 0-1.201-0.288-2.354-0.853-3.419\" clip-path=\"url(#a)\" fill=\"#435d95\"></path><path d=\"m164.08 77.166c-0.63 0.797-1.337 1.417-2.116 1.859-0.779 0.441-1.589 0.664-2.429 0.664-0.846 0-1.624-0.189-2.339-0.568-0.718-0.382-1.409-0.988-2.082-1.832l-23.018-29.433c-0.338-0.464-0.623-0.939-0.853-1.423-0.231-0.483-0.349-1.022-0.349-1.608 0-0.803 0.159-1.549 0.475-2.244 0.315-0.695 0.748-1.305 1.296-1.832 0.543-0.525 1.176-0.947 1.892-1.263 0.717-0.316 1.473-0.472 2.273-0.472 1.728 0 3.159 0.717 4.295 2.145l18.533 24.257 18.373-24.384c1.009-1.345 2.399-2.018 4.169-2.018 0.755 0 1.505 0.134 2.239 0.409 0.738 0.272 1.41 0.661 2.022 1.167 0.608 0.505 1.093 1.094 1.453 1.768 0.357 0.673 0.538 1.389 0.538 2.147 0 0.676-0.106 1.275-0.318 1.8-0.212 0.526-0.506 1.021-0.885 1.483l-23.169 29.378z\" clip-path=\"url(#a)\" fill=\"#435d95\"></path><path d=\"m243.26 74.072c0 3.662-1.979 5.496-5.935 5.496-3.92 0-5.876-1.834-5.876-5.496v-19.923c0-3.075-3.433-4.611-10.292-4.611h-17.354v24.534c0 3.662-1.979 5.496-5.937 5.496-3.915 0-5.873-1.834-5.873-5.496v-42.467c0-3.621 1.958-5.43 5.873-5.43 3.958 0 5.937 1.809 5.937 5.43v7.389h15.208c16.165 0 24.249 5.008 24.249 15.026v20.052z\" clip-path=\"url(#a)\" fill=\"#435d95\"></path><path d=\"m300.99 66.363c0 8.629-5.915 12.944-17.744 12.944h-31.067c-3.581 0-5.371-1.724-5.371-5.177 0-3.574 1.79-5.367 5.371-5.367h32.394c2.906 0 4.355-0.862 4.355-2.589v0.788c0-1.644-1.746-2.463-5.239-2.463h-17.113c-12.882 0-19.323-4.169-19.323-12.507v-1.089c0-8.081 6.187-12.123 18.563-12.123h8.526c3.659 0 5.496 1.768 5.496 5.304 0 3.494-1.837 5.24-5.496 5.24h-10.358c-3.324 0-4.986 0.716-4.986 2.148v0.205c0 1.517 2.104 2.273 6.313 2.273h17.431c12.168 0 18.25 4.232 18.25 12.692v-0.279z\" clip-path=\"url(#a)\" fill=\"#435d95\"></path><path d=\"m413.21 66.363c0 8.629-5.915 12.944-17.747 12.944h-31.067c-3.58 0-5.37-1.724-5.37-5.177 0-3.574 1.79-5.367 5.37-5.367h32.396c2.902 0 4.354-0.862 4.354-2.589v0.788c0-1.644-1.746-2.463-5.239-2.463h-17.112c-12.888 0-19.326-4.169-19.326-12.507v-1.089c0-8.081 6.187-12.123 18.567-12.123h8.524c3.662 0 5.492 1.768 5.492 5.304 0 3.494-1.83 5.24-5.492 5.24h-10.361c-3.325 0-4.985 0.716-4.985 2.148v0.205c0 1.517 2.103 2.273 6.314 2.273h17.43c12.163 0 18.252 4.232 18.252 12.692v-0.279z\" clip-path=\"url(#a)\" fill=\"#435d95\"></path><path d=\"m355.48 77.343c0 11.283-6.314 16.925-18.945 16.925h-9.221c-3.408 0-5.116-1.768-5.116-5.308 0-3.492 1.708-5.236 5.116-5.236h11.491c3.245 0 5.056-1.473 5.436-4.423h-18.177c-14.358 0-21.533-4.651-21.533-13.956v-21.2c0-3.578 1.979-5.366 5.933-5.366 3.916 0 5.876 1.788 5.876 5.366v21.2c0 2.275 2.78 3.409 8.335 3.409h19.061v-24.608c0-3.578 1.933-5.366 5.808-5.366 3.957 0 5.938 1.788 5.938 5.366v33.197z\" clip-path=\"url(#a)\" fill=\"#435d95\"></path></svg>";
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Container"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Navbar"], {
        light: true,
        expand: "md",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["NavbarBrand"], {
        href: "/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(svg_inline_react__WEBPACK_IMPORTED_MODULE_8___default.a, {
        src: svgSource,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["NavbarToggler"], {
        onClick: this.toggle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Collapse"], {
        isOpen: this.state.isOpen,
        navbar: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Nav"], {
        className: "ml-auto align-items-center",
        navbar: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["NavItem"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }, "Fale conosco ", react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
        href: "tel:08000070017",
        className: "text-primary font-weight-bold mr-3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      }, "0800 007 0017")), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["NavItem"], {
        className: "pb-2 pb-md-0",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        },
        __self: this
      }, "WhatsApp ", react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
        href: "https://api.whatsapp.com/send?phone=5541991886054&text=Ol%C3%A1!%20poderia%20me%20passar%20mais%20informa%C3%A7%C3%B5es%20sobre%20as%20solu%C3%A7%C3%B5es%20que%20o%20VHSYS%20oferece?",
        className: "text-primary font-weight-bold mr-3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      }, "(41) 99871-8914")), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["NavItem"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Button"], {
        outline: true,
        color: "primary",
        className: "rounded-pill",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, "N\xF3s Ligamos"), ' '))))));
    }
  }]);

  return Menu;
}(react__WEBPACK_IMPORTED_MODULE_6___default.a.Component);



/***/ }),

/***/ "./components/Modulos.js":
/*!*******************************!*\
  !*** ./components/Modulos.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var svg_inline_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! svg-inline-react */ "svg-inline-react");
/* harmony import */ var svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(svg_inline_react__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/Modulos.js";


 // plugin para inline svg

var Modulos = function Modulos() {
  var modulo_nfe = "<svg  xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"width=\"56px\" height=\"55px\"> <image  x=\"0px\" y=\"0px\" width=\"56px\" height=\"55px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADgAAAA3CAYAAABZ0InLAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wUCFDs21sr1egAABytJREFUaN7dmstvHFkVh797q6qrbadtJY4fMZMY2yQZDePRkEg8FsOGWSANYjcLpPkXYMWaHXskJJYsWCEe0jDAJguQYGaBhgSJzAQ7sZ2AFcZ2nJfb7nree1hUtx/tbruquonH+Vm9ctet+u49v3POvV1KRMipCvB94Nt5L/g/SwEmTU19ZfXx+urD577WNJRSfwLeb33JLTDgD4EfAf5Jk+3X0oNN1jfraK1QCoDvAueAnwOonCv4LvCrk4Zpkywub8QLy+v+0ICP6+r2//8A+GlewH8C8ydNtF9hmPDh3+8D4Dq629fe1TnG8oHpkwZq13YjxlpBN+Oyi97LA+gA5qSBDknB0WwAnM0DKIA9aZ4OfHkU5wE81XrZAdWpBbRWsFaOC9ULRQr950oDVW8FeD8xVvmOQ4dqNwJ8nKcODgCrwOhJQ7Xpl2uPtr536/ZDKp6D5+lOkIVatV1ZK9y595AkMThO71EeRQkXp0aZHB9BRFjbqGOsRSmFtYLnaSbHhtsvG5ocG+bavOLm7VXA7QhZCtAYw42/fEK9HlL1e4/yzafbfOftN5kcH8GKsLC8ThiluK4mSQxnhnwmztdQHQrf5FiN6/MXu0KWDtFGEFNgJ3KkrAh+xaXiZZMVJwZEQClEBKUUFc9pv+z3ZI01AGuP6ty8vYrvHYQsPf2DA5W+wHVSB5hj1W0lS3vw9sJq3zwYRjHTr5xnauIsIsJ/17cwpulBsXiuy9TE8LHjHIZ0ynvwzx/dob4d4ffBg4+f1nnnW28yNXEWK8LdlQ3CKMVzNXFqqA1WuTDe2YNHQabGlvdgGCVZl5qzKTxKIoLnOrhuFpppatj/VArVab93wIPtWntUZ3Fpo7wHq77XO1kXtUB70eRYjbFzQy93L+o4+uUGhNKF3vLhx3eJmsW4V+00Il678gXmpsexIqz8e5MktThaYazgey6z0+coY/jSZeLeyhrbjQjf6z2LPn2+w/joMHPT44gIG5s7RHGK62gSYxgaqDBz6VyeHfwhneZm+8gs2tKp8mCZztAt2k8qpUgSw1//trDbEPeq7UbE61df4crsJKmxLD/YJE0NWmuMtfgVl9npURytsbbY87pFL3CczPgPHj5mZyeiUundg8+eN5iaOAtk/n7yrEEcGxxXkaaWwYEK1oLWFG7wVZwceyJ4wIMtnzuOzkzfjw2F2juCAND6cDaxVtobp1weLByiVrLD1t2M1odWrQWVpAalFK4+HPapsbtbpyIqDOi5DkGY8LsbtwiCGK/E1qZd9e2Q62/McG1+miCM+cfiGnGa4miNMZaq73JldhzXzTxZCNAWBBQRrLUYY0mN7RhORZUag7XZComAtbZ5Yia7oZv9T5CCR9BqpxEd950DHhTJzD5Q7e+GNzWWOE5RCjzPQe2LfRGhQ67I68FiD5KtoMKYlh96zzJKKYxprppqjs1eUpHmxApyADyPCoeo5zkEQcwHN24RhP3yYMD1N2b52ldm2aqH/OveGkmahb+x2XnN5ZnzuI7GFK2DRZOM2Gw2XUc3P70Duq6LUorUWGzzsEkp0EohStCqGTlSog5u70RZekZ1S/ltHszCqDZURaly7VO7tIYwSgmCGK0VlYq7V2Ob94jj9FAdlDwerO+EjJypEsZpfjsJ7ARR0w/982A2gZC0JZQ9Dx56jGPlfrq4xpevXmCkVqURJsdauOI5NIKYD/54i0YQlTria9dWPeCr177EN7/+Kk+e7bB4b50ksVlbaCxV3+PyzFg5DxojfLL4Ga9fmWS4ViUIkyMvaN1gcnyk64a3aNiO1AapDQ1kYSgwNOhjTNYxGWupeE6zPkphD7q+7xBFhk/vrvHa5QlqTchu6TiOUxxH8/Zb82it+nK6rZUiiBKebwU4jmb24uhefVB7YZsam+t36wOAIuD7DlFsuLO0zqtzE9SGfMIo7dpnGitsbQddBy3b27QmLGomlI4q2Mq4kM2QX8kgF5bWuTo3vgdZ4on784tFf7RroBakscLiygZb2xF+xd3tD0/r58Budf9K3r2/weUvjh0brqXXs+gyl7z/oe14CzKODUsPHjE3fT6onanaVp3qE96RgK0GIjUm+xWtzITsA3yHrFuJ90NWKg5xYuT+6pPq1MRIVSkoWIKyhy0zKc1uaaDq4WjVLPTlCF3gD51vkhV1Y4T/PHxa6kyyrKwVtFbMXBrFqXrYktHTAgyBajfI7DXFF0iX3TnbSIsgNvv0AnisXjSfUns+bO1eOihXj+gCG8ClF4twPKAVITWWihKsHH7hR3K+P6eBX5w00GHArMluBEnzdUk5+JcVuY/yFEIN/Bi4edJQ7XK0Zms7JAyT3RNtsVnIWmFZhJ/lKfSaLMl8A/gJsHnSYC21zknXNutEcYoCrIi1Ir8FeQvYyjNOK8kkwG+AGp+jt3uVyt6ZSVKL67sgEgC/Bj7LO8b/AMa5PdnIYfUHAAAAAElFTkSuQmCC\" /></svg>";
  var modulo_controle_financeiro = "<svg xmlns=\"http://www.w3.org/2000/svg\"xmlns:xlink=\"http://www.w3.org/1999/xlink\"width=\"52px\" height=\"50px\"><image  x=\"0px\" y=\"0px\" width=\"52px\" height=\"50px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAyCAYAAAATIfj2AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wUCFQQMSdpswwAADCNJREFUaN69mmlsXcd1x39n5t77Nu6LKFmSZcmLvKmObCdR3cKNDceN66Sp2zQNmqJoUqNNPtRonACu47RAiyLd6ywGGjToFifp5qaFE6FWlMgo4touaiNxE1u7bEmkSElc3yPfcpc5/TCPIimR4iNF6QCD93DvzNzzn/+Zc2bOjPz2Zx1LiSoEFqz1/5eRG4DNwNXANqAf6AVKQNis44AGMAGMAaeAw8Bp4GDz+aIiIqgq9XoNVUVEFq0XLKvmxWUd8FPAduCdzd9eoKfF9nETzGngFeAHwGvAy6tVaDWAtgM/AzwE3Ap0X8KARHhWNwN3znt+DPg28J/AbiBrGdASzJ2Tee/fB/wmcD9zJnS5ZBvwsWY5DnwV+FvgmDEGVV2g2HwxF+tVFYzwgLW8CDwLPHgFwJwvW1R5whj2R5F8Htigql65RUowfGbpnjJHWxTyjYE+8oUcJGlLzmFNRRVyOUEM0fE3Tz4yNHRizBjzB0s6hXp80f5sPUZqdbJ1vUz0dNKuSi5t2aIvTUSEYhEqlUb58KEDjZGRoX5rbdGb3eJtAntRo8NZQ5Jl5IZO0zZTpTzQRz6KaE8TuFxkqUIUCTYgHjw5XDl06KCt16p9+UIBEanN1osz65WYR1YrXk6tBWPIT1bIVetMDvQx3t1Be+YIszVma5aVajWdfuP1g/XBweOlMAwKxVIJr73K2ek2ao0cA51TGONA5xC17LZFIAqRLKN7cITqTI3JgV4KYUBbkq4dK0FAOjw8OnXo4H4zPV3uKRSKxhgD6kicZaJWbNx+1SC3bzvGz20/SpRPUQ3Oeb0VxyFrwSjF8Uny1RpT6/sY72ijM8uwmVtpb+eGi0IRkoabef3goerJE28WRUypWGoDVQTHVFzgrYlunrh7b8cnH3wWCg4abWANWAExqwM0jy0TJ3QfH2a6r4uJ/h4K1lJaiQmqgrVCLkc6OjpZPrj/DabKEz35fMFaa0EdinCy0k1PVOMLD/wrv/LOl36LJNeplY5PEmoNK2DM6hmaL4EFp7SdHaMwU6N89QZsLiLfiJdvqwpRTggt7uiRozOHjxyJQNuKxVKTM0clzXO22s67Nh3kM3d/k+u3HYN6d0nj4scJ3E+C/BKwf4FOi9q/grFe4eXijhGIImytRvfxIdxVA9BW9DEryxYJ6OobFQvQqM+w//X9cnLwdGcuyhMEAagDhKGZbgom4fE7d/Oxd3wHiilM9KOBBesAswN4Ffhp4HvnAC0Wn0SgMuMVWtfrf5eTMIRGjDl2Agb6fDtjIEnmQKlCGAlhAGdOn+TggUNUphtSKJS81eCoZhHDtS529R7l02//Bm/bdgDSTrTc01yjLPDTBWAf8Hb8whY5cepCbY2Bg28KX/smHHqLyYFeOoXW4o5Tz05bEa5aB6U8NBJQFfJ5yNIGRw7v563jpxAJyeUixLtjTjc6ERU+eu0+PnXbt6C9CkkfakIIBQLjizVzzsDPoWFErgOqorr08Kcpt/3G78rL/3eI/Kb1EAatL33ixH9zoA96u/3knxgb4cCBA0xOVcnnC1griCp1DTlV72FH6SSfvuWfuWvrq+A6Ue3w6/HAzoFZHBAY+TvgoxInSwASPhFa/mimSu6prwnP7oNiHro7PAvLiYg31TgVNvSDJMf44Y/24zQkn88hKIJyNu2kkYV8eP3zPH7jM+Q6JyBd51mJDISmVUBg2L4YoBzwFeCDqhA119b7Xhb+6h9hcAQ29Le8i8WpkI+gOvkKp4bPUiwWQR0plsG4n+uiYR7b8nXevekFCIqo64GoaV6hXSEg+bPz3XYR+C6wa3aU48SDuneXsuMG+OJXhb3/DR1t0NkOroVgai2EYYi1BtGMcddBJSvxgc7/4onNT9PdNQzxesgiiBxgW7PrC+We8xl6EfjxxWrOZ+tbzwt//S8wNgnr+/wALcWWU6FUgEb5BxwfOsNZu5mNZoxP9TzN+9ftgSgH0nfOvDS0npnVMTQ5n6GnlgJzPlvvvUf5sRvhC08LL7wCPZ3QVlqaLQtUtMTxbD0PFV7iMz1fZmP7Ucg2QFKEaM3W7dEsQ+8A/mclLcPmUPzTbuHv/x1majDQ64HPZ8upYEuQDn2fnz3zOT6yZQ/kDdgB30loILK+XApDImBNxQDtwB+udCjixP9+6EHl808ot233DqNW9+zPSqYwncCjO3fzkRv/AyYtlNvwGa013FFpBibE4JMf715pexEfQNMMtm+Fp35PefiDUGvAmXH/fpatoAabdjr04fuZes9dzKxbR1rJYLrWmldpFVBQxAD3XlI/OsfWwx9Q/uIxuPZqODkMjdizpQL1siAmpmNXD/ILt1K991ayazZBZjywS9kpugxMBGE7BthyqYMzy5Zz8LablC/9vvLL74WpCoxONpkSgVhhokZRanTsaEfuvxa99ya4djNoCNXG6oBpCvkusHkCBLNWppw5b4JRCI/8qnLnDuEv/wGOnASXOb+mFEFToBwjpgHrA3TDRhjtQ06VYaziJ2Ix8E5hOUljKHRAoQ80I3BOnVku27hCtuLU63LXTuWm64TPPQ31mRrURkE7QAIQ9bY4k4HMQJdFe3qh3AVnqzAxDY0aSHMfs+gIxpArQvtGQEAzZKrS2FvIB/ed727XQnwwFkCoDL9G6ey/IeOvgYsh6gETstDTqV9V2xzUBJ2oQbmKpFXIWcjlIGi6ak2hWILua3x9lwBUZGS0urerPbovDAxZK6vOVUgYgGJIARn7Pmb4OWT8FSSdRqMeMLkLgVkgyKFJgFQaMD0DWcPvsS3Q0Qfdm8AGkCWzDStyYriyt60Y3tfVniNdfZajNTHmXPJZJt9AhvdgRl9Gkgk07AZbuBCYUQjykApUKpACnf3Quc6bgFuw5a4EADO1hEIuIBcZ0uwy5nqd8/FUDNp1M67rZrTyJnLqOczZF5DaEBp1gZ3NwQk4gUbDD0SbgdwAFNZDWj23XV84ZkbEKUxOxzgHxqydg1hS1EHqkAy0fSvZ9o+T7fxzsi0fBluE2hCk03P1pRnMMucDaJY0J/yFCQsDGGuEOE4ZLzcQYC29XkvAUtDSBrLrfo1s55O4bb/unUZtGJLyLKpZnS8iYgL80SDWGqr1FAG6OnJYK2SX0/wWiIMUBEELPbitH8JtfB9m5NuYkb0wfRRs3rPH4oMtmpHZNmOYt8oOrFCtp4xN1IgTRxAYrhRZXrTJmIOoRHb1Q6S3P4m74RG0dA1SP42kFR+bLmjawNmOH8ng6enr8cd/18y+yzLFGKGjFFIshFgjZM5d8bMhP8reLRp1mJE96OkXEXE+OM8bCNGYWuHWRw3+FPqz8/uw1ietJioNRidqTNe8nw+suTJOoykiYJ1inZI4w5n+B5juvAdJx+bXQlydzPbUG+Gmv5k9Hfoy8MOFnQmBNcSpY6JcZ3SiTnkmJkkdRgRrBWNkzU1Smn1bI6hCrZExXm4wOjbD9AR+lTHP5EQTEKjlr/+EmlJ5Pm8/D7zBeWeo1vilS5I64kqMsUIutOQiQxhYgvOAqc8ZorPu6Hwzlbk/zbWqf6Y+PZY5Rz12JImjEWckqTd1Y4QgoJmUnBUH2qCR27Y7Dfq/ZFxjQbL+CD5P/F0WcSWzpqaq1Bqp35kaIQiE0BqCwGCNH1nTLMjih9Wq4NQrmmUO5yDJHGmqpJkjy/w7kYvFxQzJ6sT5bS/NFO94v6hDRS84fXgeuA94hiXuH4gIdh4bSeKIEwfzFBBpxjLx9eerpKpzh9YomdNzAV+aDF58ngpogmiDRv6GvbXCzQ+CzZqne4sep+zDJ02+wkWyQDC7zV74ca+ski3nEsUbnYhgVpCGE62DiajnbvnjONr0ODj/rJnLW+rI+AhwF/AoMN765+ZAGrNMEc/kSnyKKKRB3/eS/C0PpEHf48bVEM2Y34tZpo8ngZuBPwUmVwLsMsiLovqLSbj+7tSUnjNaXbTScoDAXyx6DH/H53eA/72CIKaArwPvAX4CeEY0QTRlKW5XciR5BviTZrkD7+bvAN6FT/CvlRwGXgL2AN9pfrdlWe0Z66vNArAJf9loF57Fq/C3q3rwnjJ/XlsHlPFz8wzwFjDM3PWyo/g7dauSS70vBzDYLCPMXQDcir8A2MeFFwDr+Pk42gTS0gXAVuX/AasdCVC0wjrTAAAAAElFTkSuQmCC\" /></svg>";
  var modulo_controle_vendas = "<svg xmlns=\"http://www.w3.org/2000/svg\"xmlns:xlink=\"http://www.w3.org/1999/xlink\"width=\"69px\" height=\"40px\"><image  x=\"0px\" y=\"0px\" width=\"69px\" height=\"40px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAoCAYAAACo5zetAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wUCFQQWtLiVuQAAEO1JREFUaN69msmPZtdZxn/vOefeb6qvpq5ud/Vgt53EsWPHcUISLEOCrUQIRWFhscgiZIGElBX/AEgMK4TEjgVCYsMmYsEgJBSBACGBIhIHZ3DsJHY8tLurh6rqrqqvvunee4aXxbk1dBs7XY6TI93ur6Z7z3nO+z7P877nim7/Oz+/oaAJUFRc/jo196HpAYVFNBo0GpJHkxc0KimgGhIak6a4i4Y3VcMOKYAGNAVIHtUAyL3NIjXY3jq2dxYN0/y3Keb7aYDU3lc9qhH3c0Tk+LT6xPoZxD6MKZ8gNh9B/SqaBJF83YHk4bUFvAR8H/gW8J1fxGx/3qA8C/I7SPE5qmvn0vga0hmCn5PCHDSAJmRwFhbOQvKIGFQFogLpQ8CvHLvf/wJ/Bfz1yaeiJwHl3kLwhBMYtJP/MgiIQWdbsH8ZNRZZvIQsXYIwg3ofFQP1CPwcTR4VQVyJuA6oQX04uPEn2+sL4uyXEPHvulbJ/2g9Bz0JKEXvfQZEQMOfEuovtwCBRuitgZ9Cs482+2j0kBoINWl8nehnSIyoRjQ2iDi0M8AunkW6C3mBmm8vzj4Xp9PPadR/EXmXeYgQpyPscA07uC8/815AiZsvva+QaGywC2fOyvIlaCagDSjI8AGkfxbqXbTaRf0UrEXKIQYwtgDXATGk/Wv4q9/B721g967TeeiXEVeAUUy3pL56g9l3X1MwYN+OSg4QIezuUl64yNIzH0UMaJjeGyjN5f9+X0EhekI5eLV48Fexqx8E14NY50sM9NaQzipGEohBqh3C1oskP0dsB02eVI2RTh+jEbd8BjNcBE2kyZz6lQ3mP3qLOKl2pLBvp4o2ZcJoh865+1l65ouYjiNNR9wrVTikeH9BKUo0Ns6//h+knVexy5eQ/ikoFkAsIoC1IDZLIJp/J87RFDFGsGIQW2Inm6TZGH/1GmFvRHNjk7gzBgTTLTsZkOOoCAjEyR7dBx9i9YtfAmdJ+zsgcs9c68QGxJZgO/k7JyCk/3cYQb2zRE8ab6CTq5juANwC2EGOHNM+yxQtYCtIsQLVmLB3A20q0nxKms2Jkwlpf4/UeFQs4iwgFBdXehgD6dh8RUjzfboPPcbwqS8izhLHe2BM5rV7HK689DRa7aCzmxkY87OptIgh+Ubj/h4C5BBPUN9G/RYxJIgRrSvonMLd/xRJBWMLwuZl6je+jUiBJskXIK5AVNDag3WUF0/h1pcmpHS0+yKkekw5+BD9DzwLSUmTUQtIOtEanFn7BKSGtPcK6fZL2em5LifR9eNDVZGyTPbUWbSZZ8VRn3dUDWIsSAFqoOiCSEsDiliHKXtZhkOCmEAVJSG9DsWZZdzqIqbv0BTjnYBMsOUyvUvPQsxfvxdAABzNCEyBWX4E6Z0lbb9AGm8g3eUcNSdOpwRJjJQdZLCcuSN5qGdQTZC6Qn0NIdwJ/MFH58B2MNYh1kJRIN0S6ZVIYSB41AfitBqimj1QMyfVM3qfeAYp+qTp7RNxyNtBQaCtJ6SzjL3v0yQtCLdeQ8SALU8WNTEivZ7YTh9NCcoeDNZgpYOECpo5EhpSM8OYHmbxYk4RcWhpKHoejAVrECNoihADhAYNESkcYWdM9er1hDGob9BqxtLnfwu3dgmdnIxU3wGUY6OZgC0oLj2DuCH+jf8CVyIHJHwvQxUNPol1yFL3KNpCnVPTFmAMYg3IAMo+B66X0MN0u5k7U0SDb4NBwFrUe/z12zRXtklVmIpAnIxY+fxz9D72NLq7dbINvCdQRHIFWu3hzj+JGENz+RtorBF7jzzTWsw02sE0c2TxFCysQWcpL5wE2BwNYtrvcfTZGCQpiKAKOq9IsxlxNCHsjEjjCk2CKV0njrbpffij9D75WRjvQkocWd/3C5Tju12NsKcfptNfI1x7Hp3dQsrBT3+eEZL3RlPI3LFzFZncBNtFpYPaAozLaVEuo7KSbykW9rYJNzdQn9Dak+YVqarReYP6SGqtu1jBne12uw9/iuGnn4OmRr3/mdPm3UE5AMbPMIMVigc/S9x6AZoRuP6774QI4r2m8SgTbNE+opmC30djVhVtKrSzgsgwV8XGkbavEq5eBbGtJLdqZi0aFRoPrsCtDyjXl18bfOQ5KBbRarqC6zyBpkugy6iuomFIbApi3TZyCMAM2AN2gA3gxfb/ewTlAJtmgpoCs/Yx0q3vQbMPRf+dVamVZLN6Fg4kOTWZzFu1wLYE3kYNSkuuRTZnatCkrSS3Px50ceurmKGDIjTl2pOfwSx+Qfdv/LbWux8Q1zut0ee6SgOCZGvRXQLatBUBvcPqj4HXgX8E/g744T2Bkn1zBcZhTn2UtP19tB4jZZ+32+x2pGSk7MBgPYOgEeopVFOknh+T5LdHGW3LwFoL1mVJ7pWYbolqjbF9yrUnjF26/2+Imf/S9ncRY5HOMrjFzIt+Cr0VjD2DpibzjZEs8+pQP4cYhsCT7fWHwJ8Bf36P9lVyQWcK5PTjxCvPo9vXkc7C20DRGDHdbpZkbY1gdwjLXSTU4OdIqEn1DGP7mMX72wByaGkpug1Y16oTh5KcqgmmWKT7wLNId9FR7bdkrYg48FPU9ZHhMqSAGEeqx/iNb0Ks0BTQlDDFAO0Nsf2VHKm+bScoFpHfF2fXT+DpW2BsgTv/cfwb/0Ma30bKu4BRJR1I8vJ9R+kR2hQyDgrJHsgMcipCjqhO51CSD/0JZGeaGtzwAjI4A9PN7FRjRBYuIMUAnW1CtUe69WNUfdYg04Wii+kMwVhSMyZuvoL3M2wxoDz/OPRXwE+RTrYOfmvvkRMWOgK+QoqS8gNP0bz5PDrZQYpj5Hsgyfu7iK8ww3GW5O5xSTZ5l83dkpxlWlBUJNcvVUWazvA3tok3v4VbOocsXIBmBmGe06K7inRPQfLYUOWyQANxfwPxUyh6maPE5U0JHpUGFYMtCyh7pGrG/EdXqC9v7py8+hNBmypHzP0fJ177AdJMj+olEZL3oikgvkZ3N5DJJrgOKiVqyizJMUK5hMoyqnnCOtoi3LiKeiXVNTqvSVWDzmviPGBiQ/Pq13GnH8MsPZD7vQftiHjUxlRAmzGECq1G6HwHUBRw5x/HNPtICpjeAnG0h9/aorlyA7+9Cxh9byWx5IjBWuyFx2DvrawwYjOZNYE03juSZJG8s358pySXK4hdykJmHLq9QdjYyAClg9MRRZwDCbmKT4Fw7ZuY2y9jBqehtwpuIbtuU6LG5ueJwa49igFITa6PfJ2L1GgIu1uEN75HGE1I0zlgkLIAEffe+wQiEFsgFs7C6AoQITmkLNWeOpvbA+FAkmO7yraFaAtwxyXZtQRrs2/RLMkKiIJdXqA4vZKfa3v5rGayAeMrpJiAEqXI9ZbpYtefAFNksm8amjeeJ83GJB9Jtc9ZnBQVg5QladZghl2K9eWf9YgjlwWIRburMN1CjAJOKMvckxWXjzKaGVTTXC37JrcSxBx1CCUbODodjGv7OtZCWWC6JZQO1y0wIaHzMZrqtmAtMmohQqigqUhSYWMAzb0cjZFUzVDfIKbElIL6iMYAKSHWUl5cw51dwnRdeH/OfVKE3hIaauLNy5j+gthTgyNJ7qxkSY4Nxs+hDWMxfcziA3dWyd36SJLRQxXSpiZ5jxssoYNVNDQQ59kg1tOj4zMEaQtKCNkjpXD4M1RzfdXvYJYcbtDFLPQwvZaAQ3yHNpvkCaHSqokpIC2gUpINn+RcObTOU4JHFlaRlTlxe2MX5zAr662XaFMt+nzvotNGxiA3miCnYdnBlCWJtmWQ2haiMZlEQ/u90iD9062cK6aeofUM/DxHYpKseFIgJCR43LkLLceV+RTBGnAGiGjIx6iqEEZz+3ZQVB9B49MY9zjGPUlsLkDsgvQw1pGiQRFyVkagBuagW8TwfVk5859STx/WvVvZoC2vwcLpI0nOIGc5Nu9QJbfWnhhzUTid4bd2cQUUj/Za3kk5Aooe9AroLCCaIDUIBlk416Zuglhj0/ls8JC2P+NJMWROdpZUNTRvbeNvjg7TpwP8LiJfAftLuvcTp80+0llA6yka6swLYpDlB/MCQ4OoRdXTSsVDqD5Fil81a+fQhUV0uovutJJcdFEtUFu26hKhs4yy1Ea2Qfe2CNc3ckVcN2jVSnLVEOces7ZAHN3GeI8MT+fUlOxqc782G7pM+KHtIqQcBSkibeocXklI0xlhd5+wuUcYTTmQ5CeAvwUeRQyaKnR6E+ZbaNFHVh6BYpgLQT/Np2yzLWjm+cTeFojtIEUXoqDNNE+y14fSobMSqac598MEDZob102NlsuIXT6S5FvXCdcPJPlYlexcm6m5n6v7t2E+Qvqb0FlCXac1ZjYHsCnaglUzoccAdUWcT9AQc49mPiNN5sTJnDRvQCUXoyKFwxb/RGgu5adpDr3hRdQYCDN0egNlC40VEj1h9FZ2kiqkFJDYkDBIbwm3tJ7bj7GG4MEKqd/H1LM8sXTMzVp3V5WcF3UkyaltdgMY7KkhxZkDo9c2u+cjmO1kTsCh6tAYUNtFzvdBynwM21T4N36CzibZGTQhy3ECFcFYS5w3mMUexfqyOq1GF2W4DvV+G3IGWfkwMrwIzR7UI6KfI7KAYLCakKKXFwTEzZcJN18m7m5APaG4+CQqFtMzaKjwG/vYcx9CbITJCJoKmjov1trDsiD7DwedbivJFgqHFCXSKaBjcWWJiRGdT7IkG83p4ywEzUTqm/zZ12Dac+zQ5MIvJcQ4KFw+tA8xi4mzlPdnSZaOi6557d/m5YOfWZDh+byLB0ecpoDeGeitYTRlZt2/Qrr9Y0ARHCnWaDPD9FcgzLGn1jH9HqlOhL1d5j94hTRPdB/9NNIfwtI8S3JoJVl6uUo+lGRL0WurZCMgmsuBGEh1RWpq3HAZXVhFg88tDT+D+RTwOWU0ZSAOLLG2xySa2k6HgrWYTgeKgqLXRRZ7mK6FpiHVftGl2Y5tXv1n7OqDmMULSGcFysFRTXEQ7nGGlD3smY+AnyEoRkzebSDsXiNNa6rd1wjbt2g2bxF3RhQXLmSL7ufZXEGOslLB9O+U5KJEiiKvrQVDUyZKkoKRKTEMsF1kYQ06ucI+lORmDs0MSQLD01mSVZHY4M5dvEuSJUuyZknWuslNLfiWk2LQw0Ti7uvo/uu5FWAHqO0hNqeJJkW6y0hvBaREukvE/W3S3nWSb4jTKVrVxPE+cTw+JE4z6EEMLxHSCzh+A7jvQA2I+XWtQy8ireGK8chkQQN8D3hR4e/FyIuIfA1Nv0ZsILh8GtAdQjk4lGQwyML6nZIczx2T5Hx2pI1v/UkC2Af+APhLB/oniP0jcYNck2hEqx1SCGjIk0yzfcyZJ3BnP0aq9pGyj79+mfDWd6BYyPyZsqyafg+NCY2Kinwzzee/qT7ckp4sAxeADwIXgVPAkqouKgddeDNG5DLKJvAacBO4epeTegb4PeArwKc4MHkx5rSJTUsDPqugphaEzCuKtJF3eHL4beDr5JeMbtC60z8mN3C/Cvz6Qc0hFCAJbEKi5reKhNZwKVIUSHcArovEtnjLfdUG+NdW5r+Wf18gN4z3yO+w3e0Yf2olcdf4i/Z6jPz61yeAc8B5YBlYBE6hKq3998Bl8jt019rFfxt4Afjx3Tc/MG//0F4H75g9DdwPXALW2gfZoz+zZKLgKnALeBN4C/gG8F3gyklX+R7Hy+11fPRA+iiLilpUFBUPbJLd908dd9v8n5B38y3gwRak+4BVoH/QCtdszSegb7YPe5V8VPASMP8FAfJOY95et9/rDf4PEGiDD25hKCYAAAAASUVORK5CYII=\" /></svg>";
  var modulo_controle_estoque = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"67px\" height=\"50px\"><image  x=\"0px\" y=\"0px\" width=\"67px\" height=\"50px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEMAAAAyCAYAAAAHtGYXAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wUCFQQfzWQtHQAADYxJREFUaN7dm1usZGlVx3/ru+xdl3PrPt0zw0CGAQOoBB9M1EQTNPhgDPGS0cGAEhEN4cknfcGovJn4YtQn5RKIOBjABFFEgzE8yMRAMjg4GgeYme5hzunuc606ddm193dZPuw6p7un+3TXqW5moivZOZeq2t/6/t9/rfVf3/5KDv7tXxBVljGNgt9scBsBARCYfbdLDgYxC93zLcAHgZ8GPgv8NfD8XccNgjsXKF8zQ4NBE9RXO+SZRezZ5yJAyuCWQuHe7aeA3wLeBfj5/94GfAj4hzkofwcst0pL2isJRgE8DnwAePsp7+kAvzy/ngc+RcuYZ14JB80rMMYjwO8Dz84n9/YFP/dG4A+AbwL/CPwK0P1eOnr/mXGd2D8GvB94D7ByD3cU4Gfn1zbKExrMZ3Jjvn6/g+j+MENBk4DSUZXHc22+pFH+nTYk7gWI65aFXJuHNZrfcefD18qHZt8Qq7+rWd54v8CQe6omQSgeqPGbzZs0y/tsP/66BvPasFcSDj1pakHA+Nyu75m9ayuWBoMUGb/R4C80+I2AOEWzAFRh4L40e6H/57kxX3n1qonwQ6myv11Yfa/fDAWikDNuLVLMDPGgIOwXxJEDBfFnc1STYPsRfz7g1gJuNV7ncoZcGcKh74b98jGN8phY/RrwBPAZ4MqZp7MkM36GNgQe0yiIU/x6aDXHekB85oQKCmlqqa90aPZKRHRhlmgSis2aziMVtpdQQIMhjhxhryAcFOSZQSzzMU9sSFuanwD++e5r2jLjLGCsAu8GfhP40ZuczoIGAQG3FvGbNcX5gOkmchDSyNPslMSBa0c+Q8honN93I+DXA/HIEQ4KNLWLsEBYPEOrWz4LPHevYLwJeB9tZXjozp5DjgYy2F7CbQTS2BGPHGJuWb3FAcnSJug54YxTWEzh3mgzrgu6L9DebWEwfpLrKrE48wRSOwExirhXVEguYjcJOgEUcwsYnlb9fYBWMv+/NxH5sivLj8eQviCDJ/8Vcn6dqr4f+A3g0Vfbwe+5qWKswRYFWWG0tc3Xn/6vP3F1Xf+xc+6DzrpVNJNTAllGFLySk5n/PKubqhhnsUURQh2a0bX9NDkc2qNvf6u/t33ljW4yGr3DWLtalOWwKEv13ntUS03JqeqZgdHUvn8Z8bMICBoNqi0QAohbQNBpxnqvxhd1M6vjcHuH0e6eC9Np//jDvZWV6IwxO5oz1WSyXldVcmWnLjudyntvrYjVGHxOyWDMHZ3MoX3d9SOa5br6LJarILcFQgW7GvAXGsRAs1uQjhyab1NmVQHFFWUW75vZdBZHV7dlvLdfhsnUG2vwnQ6hrokxICKlAxoRwVoLqjZU016optiibMpuLxRlGV1RGlIsUmhsu+xmDnirL8TMRdeFBn++QZMQh540coShQ7NpxdayOCTBdhKd108pLjatTE9QXKiJQ0848ITBXICZjLiM63SjOBer0VRHW9dkvLvXi1VlrHf4TjmfhiHMZuQYMSKFA8LJqCIY2843h6aY1LOicl6LXi+UnW7tu32MZpvqxqeQjXHgH5j3C+ttv4CAOMX2Eo0piSOHJpbrTY7dMkpOQn2t3c1yGwHbTYjP+M0Gv1mTKiEMusSDi5FEmE5GjHZecpPdPZ+aBld4fK87Z0w7VwViVbWtgkjnZjBucsBgjUE1yexoUNTjUeE7vVj0eqEsenV5jk758Ejs6rBNaGpAhTR2hH1Ps1+SKotxedEtwDugASQhHBSEvQLTyfjzLQvdygxcgZSb2jkfiPVzvPhU9Ec7A4cGXFFcB+EGPWVESHVNU1V4YzA5laeCcZ0sBuvasAjV2NXjiau7lgfP76nUFzTGh9X2G8mTkc6uOBOHHXJtEadtt3q/7Lj7BTTA7EVH3O+nzms70XQqL4NLQvWM6OSqm+6/DTGruMLdAsLJ7aylOToiNg2F9yDJOaBZ1B9jHSKOFGrS+FtiwzcReUTqgx/IzfgNjQlJjB1b9ZVFRMDePzAAckZzxvd6wV1cSXWVeOmpkV3vftVcePAp0BLxG9iiRw4CevvFEBFUlXo8boFqK1NxV2bcaooYC2YNbMLyHSYHU3O4V3R6m/3Q6fdjYVeD1jMbpxOnKRqx7p60i6YEqBara8F2+2k2ncnB5S13tLXlJvs1qz84gHINmnkjeJctMGMtzXRKM51irJ3j0SbQeCbPREEFzSAYVFYwRYccJxxdGfhp4Xy5tqHd1bXoz19sJCXidGTSrPKIEbELskUVTQmxJpcb56KUnVyNp3L0nefLo60tE6dTXFnieyuI7ball7n2MPnOeIgwG43IKWF9uzlv4GxhcgM3UDUtMICgiLU4Z4FAtb8r1cGeL1ZWfXf9XCzXzkW/fq7O1VTCZOQ1JdOy6zbaRTMaE6YsYnn+QsJ5poMjM3zu2c5oe4vUNPhOh2JlpaV4VHKWG9hwd1aE2YzZ0dFJ5ZybWyJMTuC9Aeg8/0vbhFu0TW6YjKmPhs51Oq67cZ7O2noo+6sNMRBGQxNnlRMRI9ahOUFK6nr9UDywmZIYGe/tu8GLL7rxtavtDlqngz0GYZ4UVQVUTtwxNmNM5rRaLtZSDQakEE781JYsS4KhgrEBYyI5H+8c3roixjmMc2hKjK5uM9696jtrG757bjOW5x9IBRrSeERzNPBuZSWVG5sppCyH13aKwaVLdrq/hzEGX5Yti06pDApgE1Jm8rTXsvY2Zp2jmUyohkOMczd93iB2qQSqGKrRBXwxxXXGWFffUVSJMbiyBFVmwwHV4b7z3b7rbV6ks7YReuvnUmwa9i6/VB5eel7qwQDrPUWvd8Owp9BfaJkQHXsvPczBtYuEusC6l6VCERBhcnBwU6644eWzgyGiiCSG+69ncvQAKxvbWBcwJpOzuduH5054cmgYfPcFXFn6C29+K1ef/iaHl16gWFlp88GC5n3D0eEGRwfnGB5uYE3CFbdOyXpPNRi2ucLdug9uwCyZMxRjAzF0Obj2ZqxtMDYhsrjIEmtxpgOaacZHaIwU/f4tK3Y3sy4xGa2g2VB2Zrd9j3GO1DSMd3cQkVYC3TSbewJjPogNGBtQlTaRndEEUBEEYeGS+zJTFaxNYNMpg7STH+/sEOoaV9x+B1MQY1iitN463pK9hwiaFdV83zaU9GWhar1nenDAdDg8lXUKGGl78bOJrnYWpFiS8/2Q2wr53jeCUnQ0dQmi88VRrPc0kwmj3V3E2lvC48QDVYyIHDNjcW9UEIn0169hXUNsuuTkTwTYMljcKzNidPgi8PCjl3n0+7/d7mmYkhQCwytXyDlj7rA5pYCft/Bn2m1QNQjC+Qe/DapMjh5icvQAoe4jJi16m5c7k6VdtjMjEoNn88EdXvOGy7j+lDzpIcaRGuXoyjaxrk8ND2lBAIFxynGpMFE1xFjiyikbDz3L+uZlcrZLJVEENOesevZnnKpCzobV9SGuP4WqQ84lIjC8coV6MrktEAYoRTAiDFMafmta/9kLs+ZHHPBPtA+L3gO8Y3FPhJw8ku1czh5L4LOHi+asLHES4Dhxh+AhOjAWMS0jquGUonszEFYEL0KdM3sx/fdeiJ/cCeETguxseIujfUj7sfn1w3NQHqc9cXMbD467VrmxO7mHbT1pMxiqS+cNFTAWMsT9KzQjwbg+x08QCxFEYJqUqzF8eS/Gj4asfwskEaErguHWIwlPza8/BH4e+FXgnbeMfVPXKnNWLJdApQ2T0/ZhFgAC1BSgCd27BtUMUzyCiYZClAyMUp7txvg3dc4fGcT0pAKr1pAVGtUTz087nzEBPj2/3jIH5V3z3+fzvrFr1XvY7xVUs0Be7jyLKyAG2HsJrSJFWdK1MFMYpHRpP8ZPXm3iRwy6dc57usbQnBKSixxjepb2oNnbgF8APqeYAIoxiRNGLC285sw4Y84QY1pBNZow2fou6BjTM9TBc6XOT/5PVb3/ct28dZzyhxG2usZguDN/z3JyJ9A+yv+CSHo01P0PNLPqva6oXicnoCwb82pEFk8Yx43WZH+famuMXYmMmxV2dy98/hsvrP/lc8P8JSuJDbHHfcdCQbzUMSaRdCk23Q9NR5t/5MvJz2n2v0ibW3pL3A1VXUzKzrveXNfMdneZHA0ZWXvtP/bXPl00vY/3Hf9ZNQYHFObsC7PkmS6Zq7w0ytk/kULnCVXzsEh+nDa3/Pjid7ohTO5ADuMcXoTZYMDBzg77VfXU1NhP7IXmU3Ysh9/Xs/T7MwrXVo5lgvaez4GK5OPWfRv40/n1E8CvAb8EXLwbGtqWktvGmbGW0jliVbF9bYfdweCLeyn/xRj+foVMIULhFGfzMlLl/oJxin11fv0e8BjtWbBTBN0JGvlGf4wxlM4RmoatnZ3hpZ3dTx3WzcdXyvKpZC1Fzq2Auo9Of6/Pjh8AH51fpwo6VVVUE+CMCKX3NCGwvbv7zKWdvb96aTT6pHP2Wq/wdIwh5MxyXdCrC8aNdizoPkwr6N4DvBOBnLM1YEvnCVm5vLv3xe2Dw4+Nq8nnQ0ZdUdAzsnBV+L8AxrGNac9nPgG8BeS9VuTdw3q2sXtw8GljzEee2919umoia2WBiZkmxlfkuxav1vdNju1Zgc8ZkX4d4uZwWn12rd992lqLt/fxofWC9r+iat+f2bVcXgAAAABJRU5ErkJggg==\" /></svg>";
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "box-modulos pt-3 pt-md-5 pb-3 pb-md-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    className: "Col text-center  mb-3 mb-md-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "pt-md-5 mb-3 mb-md-4 title-vhsys text-primary",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, "O sucesso de sua empresa est\xE1 aqui!", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    className: "d-block mt-3 w-100",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, "Confira nossas principais funcionalidades para voc\xEA controlar sua empresa"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, "O software de gest\xE3o VHSYS existe para ajudar micro e pequenas empresas a terem mais controle sobre o neg\xF3cio e praticidade no dia a dia. De forma integrada, o sistema de gest\xE3o abrange as \xE1reas de vendas, financeiro, estoque e emiss\xE3o de notas fiscais eletr\xF4nicas."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#",
    className: "btn bg-teal text-white pl-5 pr-5 mt-3 mt-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, "Confira os m\xF3dulos do VHSYS"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
    className: "box-modulos--info flex-column flex-md-row flex-no",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "rounded shadow",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-flex justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "modulo_nfe",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default.a, {
    src: modulo_nfe,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-primary mt-3 text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, "Emiss\xE3o de Nota ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }), " Fiscal Eletr\xF4nica"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "mt-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, "Emissor de NF-e, NFS-e, NFC-e, CT-e e MDF-e;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, "Nota fiscal de importa\xE7\xE3o e exporta\xE7\xE3o e carta de corre\xE7\xE3o;/"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }, "Relat\xF3rios de notas fiscais emitidas"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "rounded shadow",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-flex justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "modulo_controle_financeiro",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default.a, {
    src: modulo_controle_financeiro,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-primary mt-3 text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, "Controle ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }), " Financeiro"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "mt-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, "Contas a pagar e receber, fluxo de Caixa, DRE Gerencial;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }, "Concilia\xE7\xE3o e controle de contas banc\xE1rias;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }, "Gest\xE3o de custos e servi\xE7os recorrentes;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  }, "Relat\xF3rios financeiros completos."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "rounded shadow",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-flex justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "modulo_controle_vendas",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default.a, {
    src: modulo_controle_vendas,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-primary mt-3 text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: this
  }, "Controle ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: this
  }), "de Vendas"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "mt-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }, "Sistema PDV - Frente de caixa;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63
    },
    __self: this
  }, "Emiss\xE3o de boletos, pedidos e recibos;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: this
  }, "Cadastro e gest\xE3o de clientes e vendedores;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: this
  }, "Relat\xF3rios de vendas;"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "rounded shadow",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-flex justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "modulo_controle_estoque",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default.a, {
    src: modulo_controle_estoque,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-primary mt-3 text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: this
  }, "Controle ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: this
  }), "Estoque"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "mt-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: this
  }, "Giro de estoque; "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  }, "Controle de compras e notas fiscais de entrada;"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78
    },
    __self: this
  }, "Cadastro de produtos, fornecedores e transportadoras"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  }, "Relat\xF3rios de estoque."))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Modulos);

/***/ }),

/***/ "./components/experimente-bg.js":
/*!**************************************!*\
  !*** ./components/experimente-bg.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/experimente-bg.js";



var Experimente_bg = function Experimente_bg() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "box-experimente-bg text-white pt-3 pt-md-2 pb-3 pb-md-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "d-flex flex-md-row flex-column align-items-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-md-50",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/static/img-notebook.png",
    alt: "notebook",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "w-md-50 text-center text-md-left",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "mb-3 mb-md-3 h4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, "Fa\xE7a como ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, "mais de 150 mil"), " pequenas e m\xE9dias empresas e experimente o Sistema de Gest\xE3o Online Gratuito por 7 dias!"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#",
    className: "btn bg-teal text-white pt-2 pb-2 pl-5 pr-5 mt-2 rounded-pill",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, "Experimente gr\xE1tis por 7 dias")))));
};

/* harmony default export */ __webpack_exports__["default"] = (Experimente_bg);

/***/ }),

/***/ "./components/experimente.js":
/*!***********************************!*\
  !*** ./components/experimente.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/experimente.js";



var Experimente = function Experimente() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "box-experimente pt-2 pt-md-4 pb-3 pb-md-5 text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    className: "Col text-center text-primary mb-3 mb-md-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, "Sistema de gest\xE3o completo para emitir notas fiscais, controlar o financeiro, gerenciar vendas e estoque! "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "#",
    className: "btn bg-purple text-white pt-2 pb-2 pl-5 pr-5 mt-3 mt-5 rounded-pill",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "Experimente gr\xE1tis por 7 dias")))));
};

/* harmony default export */ __webpack_exports__["default"] = (Experimente);

/***/ }),

/***/ "./components/faq.js":
/*!***************************!*\
  !*** ./components/faq.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-accessible-accordion */ "react-accessible-accordion");
/* harmony import */ var react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/faq.js";




var Faq = function Faq() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "box-accordion pt-3 pt-md-2 pb-3 pb-md-4 mt-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "pt-md-5 mb-3 mb-md-5 mt-md-4 title-vhsys text-primary text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, "Perguntas e respostas sobre ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }), " Sistema de Gest\xE3o Online Gratuito")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["Accordion"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemHeading"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemButton"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, "1. Quais as vantagens do sistema para clinica veterin\xE1ria VHSYS?")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemPanel"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, "Sistema de gest\xE3o \xE9 um software que te ajuda a gerenciar as atividades da sua empresa. Atrav\xE9s de uma plataforma inteligente ele te permite controlar o financeiro, vendas ou presta\xE7\xE3o de servi\xE7os, estoque, clientes, produtos, contas banc\xE1rias. Te permite emitir nota fiscal eletr\xF4nica (nf-e), cupons fiscais, boletos, pedidos, or\xE7amentos, ordens de servi\xE7os e muito mais. "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, "Com um sistema de gest\xE3o \xE9 poss\xEDvel receber relat\xF3rios gerenciais, indicadores e gr\xE1ficos e visualizar os resultados da empresa de forma simples e r\xE1pida. Isso te ajuda a ter uma vis\xE3o estrat\xE9gica e tomar decis\xF5es com mais facilidade.  "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, "O sistema de gest\xE3o tamb\xE9m centraliza as informa\xE7\xF5es de todos os setores do neg\xF3cio e, por isso, consegue automatizar tarefas. Com poucos cliques voc\xEA pode, por exemplo, preencher uma nota fiscal eletr\xF4nica, fazer um or\xE7amento, lan\xE7ar contas no financeiro, baixar produtos do estoque, etc. "))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemHeading"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemButton"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, "2. O sistema de gest\xE3 online \xE9 gratuito?")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemPanel"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }, "Sistema de gest\xE3o \xE9 um software que te ajuda a gerenciar as atividades da sua empresa. Atrav\xE9s de uma plataforma inteligente ele te permite controlar o financeiro, vendas ou presta\xE7\xE3o de servi\xE7os, estoque, clientes, produtos, contas banc\xE1rias. Te permite emitir nota fiscal eletr\xF4nica (nf-e), cupons fiscais, boletos, pedidos, or\xE7amentos, ordens de servi\xE7os e muito mais. "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, "Com um sistema de gest\xE3o \xE9 poss\xEDvel receber relat\xF3rios gerenciais, indicadores e gr\xE1ficos e visualizar os resultados da empresa de forma simples e r\xE1pida. Isso te ajuda a ter uma vis\xE3o estrat\xE9gica e tomar decis\xF5es com mais facilidade.  "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, "O sistema de gest\xE3o tamb\xE9m centraliza as informa\xE7\xF5es de todos os setores do neg\xF3cio e, por isso, consegue automatizar tarefas. Com poucos cliques voc\xEA pode, por exemplo, preencher uma nota fiscal eletr\xF4nica, fazer um or\xE7amento, lan\xE7ar contas no financeiro, baixar produtos do estoque, etc. "))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItem"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemHeading"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemButton"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, "3. Quais s\xE3o as funcionalidades do sistema de gest\xE3o online da VHSYS?")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_accessible_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionItemPanel"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  }, "Sistema de gest\xE3o \xE9 um software que te ajuda a gerenciar as atividades da sua empresa. Atrav\xE9s de uma plataforma inteligente ele te permite controlar o financeiro, vendas ou presta\xE7\xE3o de servi\xE7os, estoque, clientes, produtos, contas banc\xE1rias. Te permite emitir nota fiscal eletr\xF4nica (nf-e), cupons fiscais, boletos, pedidos, or\xE7amentos, ordens de servi\xE7os e muito mais. "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }, "Com um sistema de gest\xE3o \xE9 poss\xEDvel receber relat\xF3rios gerenciais, indicadores e gr\xE1ficos e visualizar os resultados da empresa de forma simples e r\xE1pida. Isso te ajuda a ter uma vis\xE3o estrat\xE9gica e tomar decis\xF5es com mais facilidade.  "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }, "O sistema de gest\xE3o tamb\xE9m centraliza as informa\xE7\xF5es de todos os setores do neg\xF3cio e, por isso, consegue automatizar tarefas. Com poucos cliques voc\xEA pode, por exemplo, preencher uma nota fiscal eletr\xF4nica, fazer um or\xE7amento, lan\xE7ar contas no financeiro, baixar produtos do estoque, etc. "))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Faq);

/***/ }),

/***/ "./components/planos.js":
/*!******************************!*\
  !*** ./components/planos.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/planos.js";



var Planos = function Planos() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "box-planos pt-3 pt-md-2 pb-3 pb-md-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    className: "Col text-center text-primary mb-3 mb-md-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "pt-3 pt-md-5 mb-3 mb-md-5 title-vhsys",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, "Pre\xE7os justos, sem ades\xE3o, sem surpresa")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, "Por ser um sistema para empresas online, voc\xEA n\xE3o precisa pagar taxa de instala\xE7\xE3o e manuten\xE7\xE3o do software de gest\xE3o. Basta acessar o sistema com qualquer dispositivo que tenha acesso \xE0 internet. Voc\xEA pode optar por um dos seis planos dispon\xEDveis, todos eles t\xEAm acesso total \xE0s funcionalidades e aplicativos do software, a varia\xE7\xE3o se d\xE1 no n\xFAmero de usu\xE1rios e emiss\xF5es de notas fiscais eletr\xF4nicas. Veja qual \xE9 o melhor plano para as necessidades da sua empresa."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "position-relative mt-md-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "box-table text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Table"], {
    className: "table-responsive-xl",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/static/icon-plano-bronze.png",
    alt: "MEI",
    className: "mei",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-weight-bold mb-0 mt-1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, "MEI"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "old-price text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, "R$ 59,90"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "price",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, "por"), " 41 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, ",93")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-block text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, "por m\xEAs no plano anual ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }), " Economize  ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-primary font-weight-bold",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, "R$ 215,64")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-outline-primary rounded-pill text-uppercase",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, "Contratar")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/static/icon-plano-bronze.png",
    alt: "Bronze",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-weight-bold mb-0 mt-1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, "Bronze"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "old-price text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, "R$ 79,90"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "price",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }, "por"), " 55 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }, ",93")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-block text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, "por m\xEAs no plano anual ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }), " Economize  ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-primary font-weight-bold",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, "R$ 284,64")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-outline-primary rounded-pill text-uppercase",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }, "Contratar")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/static/icon-plano-prata.png",
    alt: "Prata",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-weight-bold mb-0 mt-1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38
    },
    __self: this
  }, "Prata"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "old-price text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }, "R$ 99,90"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "price",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, "por"), " 69 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, ",93")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-block text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }, "por m\xEAs no plano anual ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }), " Economize  ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-primary font-weight-bold",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }, "R$ 359,64")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-outline-primary rounded-pill text-uppercase",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, "Contratar")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/static/icon-plano-ouro.png",
    alt: "Ouro",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-weight-bold mb-0 mt-1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, "Ouro"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "old-price text-white small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }, "R$ 169,90"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "price",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, "por"), " 118 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, ",93")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-block text-white small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }, "por m\xEAs no plano anual ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }), " Economize  ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-white font-weight-bold",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }, "R$ 611,64")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-success rounded-pill text-uppercase",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }, "Contratar")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/static/icon-plano-platina.png",
    alt: "Platina",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-weight-bold mb-0 mt-1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }, "Platina"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "old-price text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }, "R$ 259,90"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "price",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, "por"), " 181 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, ",93")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-block text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }, "por m\xEAs no plano anual ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }), " Economize  ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-primary font-weight-bold",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }, "R$ 935,64")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-outline-primary rounded-pill text-uppercase",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: this
  }, "Contratar")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "/static/icon-plano-diamante.png",
    alt: "Diamante",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "font-weight-bold mb-0 mt-1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }, "Diamante"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "old-price text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63
    },
    __self: this
  }, "R$ 299,90"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "price",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: this
  }, "por"), " 209 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: this
  }, ",93")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-block text-gray small",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: this
  }, "por m\xEAs no plano anual ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: this
  }), " Economize  ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "text-primary font-weight-bold",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: this
  }, "R$ 1.079,64")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: "btn btn-outline-primary rounded-pill text-uppercase",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66
    },
    __self: this
  }, "Contratar")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: this
  }, "1"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: this
  }, "1"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: this
  }, "2"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: this
  }, "3"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: this
  }, "4"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  }, "5")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80
    },
    __self: this
  }, "5"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    },
    __self: this
  }, "25"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: this
  }, "60"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83
    },
    __self: this
  }, "150"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84
    },
    __self: this
  }, "300"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85
    },
    __self: this
  }, "500")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88
    },
    __self: this
  }, "25"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: this
  }, "125"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: this
  }, "300"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: this
  }, "750"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92
    },
    __self: this
  }, "1500"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93
    },
    __self: this
  }, "2500")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96
    },
    __self: this
  }, "15"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97
    },
    __self: this
  }, "78"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98
    },
    __self: this
  }, "180"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99
    },
    __self: this
  }, "450"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100
    },
    __self: this
  }, "900"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 101
    },
    __self: this
  }, "1500")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104
    },
    __self: this
  }, "Gratuito e ilimitado"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105
    },
    __self: this
  }, "Gratuito e ilimitado"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106
    },
    __self: this
  }, "Gratuito e ilimitado"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107
    },
    __self: this
  }, "Gratuito e ilimitado"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108
    },
    __self: this
  }, "Gratuito e ilimitado"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109
    },
    __self: this
  }, "Gratuito e ilimitado"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "box-table-legend",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115
    },
    __self: this
  }, "Usu\xE1rios "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116
    },
    __self: this
  }, " NF-e e NFs-e\x03 "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117
    },
    __self: this
  }, " NFC-e\x03 "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118
    },
    __self: this
  }, " CT-e\x03 "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119
    },
    __self: this
  }, "Suporte T\xE9cnico")))));
};

/* harmony default export */ __webpack_exports__["default"] = (Planos);

/***/ }),

/***/ "./components/solucoes.js":
/*!********************************!*\
  !*** ./components/solucoes.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "reactstrap");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(reactstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var svg_inline_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! svg-inline-react */ "svg-inline-react");
/* harmony import */ var svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(svg_inline_react__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/components/solucoes.js";


 // plugin para inline svg

var Solucoes = function Solucoes() {
  var integracao_contabil = "<svg \n    xmlns=\"http://www.w3.org/2000/svg\"\n    xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n    width=\"87px\" height=\"67px\">\n   <image  x=\"0px\" y=\"0px\" width=\"87px\" height=\"67px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFcAAAAvCAYAAACWuVbAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wUCFTEvSYzfBwAAEadJREFUeNrdmnmQXVWdxz/nnHvve69fv05vWbqzQ5AlrIGATjDssrgUSwLoMDWgoKPOuI6MTo0zWiVjFe7iuIEW6jAjo2Uhjku5IlJlKagBBMKSdNLp9N6v3/7uds6ZP+7rTkj6Jb1hqPlWnerX957l9/ve3/n9fmcRl19wG0fAccCHgAeBH7CIGBwv8darX8nNr9tM2Y+J45hiucrKpd0US0WyXgqtDXUdExpNrR6yclk7bovECokK6sjAByGS/zu6CX0fYTVWazrW9nDPNx/iM998mNUr2hdT9FnDOcr7m4FbgZOBFPDdYyLlywunABWg/2gV5RHevR34cOP3FuA7wOPA3wEtx1rDY4CzgC8DOxo8vO9oDZqRexnwxRmenw58CXgS+CCw4lhr/BfAxcD9wB+BtwEu0A58CvgRsKxZw5nIXUpipUfCccDHSb7gncAJx5qBRYYE3gQ8BPwCuL5JvSuB35O4zRk7ORTfBJbMUohlwAdISL4b2HSsWVkg2oF/IJmZ9wEXzKLNWpKPsO7QF4cGtG3AFfMQKkMS+G4lCXpfJvni01BKUpgsU63WARgYKVCpBTiOAuLFp0lJqn7E/qEJXHug/2w2Q0dnG3GsDyXoFuDNwOp5jLYM+CGJ25zu+GByJfC5RVBrW6P8r1Ly9YXJMuVylTiK2bR5I8cdvwqAiVKNV6xdTrlS58hxdX6IagEbetu54erzWd7TPf189659/P63T7JmXS+OUiC4Bfg80LrAIU8B/oMk4B9G7m1A72IoppQiP1EwxUKZTZs30tW9hDPOOontb7ycXC47Xa/26KO80LePpcu6Fp3c8liJS85ex2tvuQZS7dPPJ/Ml7vjIV3jk4T9itCGK4v7upe2trutgjF3osG8D7gEeO5Tc2xdDKSklQ0NjlZWrlm277R3bDyP0YNSDCKUW32oBpJL4YYjMl/F6DpDb0dnGJz//AZ55ajfVSo0fPPDQL37yo0e+kc1m/jaV8rB2wQR/ErjwYHJfQ5IBLAhCCIYGx9iy9Sz78U++95pcW/Z7vCQOdfZoRtbJGxN1zznv1DPOe9XpbR/98BeRUjZiwIJwAXAmsGPKbG5aDEXCMLHE991+cy7Xlr0feAJ4N0kUfrlhK/BtYMdVb9h6zeZzNzI+NokQYjH6vhWSSOKS5GsLghCC8bFJLrh4M8dtWDX1+GTgsyQk/xtN8sGXDNYi5IvcTjtJzvpz4NfADVMvtr/xCowxaK3nNEQTXANICZwHdC+ws0ZqI9h24+UzvV4NfAT4l6kHE889T1StIR2nwYMljCKMNhhjWo0xFxpjXqnjGK31iyzKWos1GozptcZssMZgrcVoDRxwA9JL4Y8OHizHiSSrrUsOFXDrRZs5c9NJTOZLi2G9vcDZU+QuCEII8hMFzjlvI6/acsaRqn4VYN/jT7D3d7/H9Ty0MURRjBDinI4lbR/MZDMP965Z/URnz/Jfda/u/e3y3p7HXMd5kx8E0wQKa89NtXc9mFq15vFU76rH0ytWPqlc78duKvVWLBuM1lhjka5HVJyk+ORjU+M/Bjwysw5w9bZLqFbrGGMWSgnA+Q5w6kJ7Mcbg+yHXbr+0aZ3ID345/PQzv57YP0hpbIz2JTlio1ta0ultubbczbnW7EVOS6YxmQCdKOi1t53d2rHkvpa+/VfpOLgrm2m9PNXa9lGpHGwASAueOFVZc6qbTl+ho7Cmg+CnURh+wxrzAEIS5scoP/UnwmpZt6xa++VM79rzZ5Lxyte+mm99/UGGhydob29lgYnDRrVh3dnvZYal22whhKAwWWLDK9bwgX9+M0rNHG3N4KBXfGZn9+DQ8CPS82RPd9ftHZ2d9yxbvvTNmY4l66XrYmt14vECweAo/v5RwrEJTL6EQrBkeefpbs671WttvUj4YPIxthRjaxrrG8CCa5Ge6zqp9EmO590opNxmrZEGHo0rZYj896SE/Uenc1kX8nA5XdehUqnxq5//jiXtuQUxCxQdFrhwsNZSLFZ459UX4nnuzJWMwQ38letOOfFDnT0rrrVgl3R3noTrQhgRj0wQ5otEhTLaDxK3KcDGhjCKMSImvdTDNR56PIKqAStAJZxSs9g6kAZyFpsCIQWOlzrVcd0vRH5wi8X6XiazBR2gi+Oorp4ZRb36uku5/74fU6vWaclmFkJNr8MC06RKpca69b28/tqLm9YJ/rwTOT6Gu7aXtp7lJyIE1OqEIxOE45NEpQo2NghHIt0kwJkwRrqK9MaVuD2dmJLGTuoka1Yk7mBq2sopksHWNaJVYHMClAEDbiZ1tpBgjcHUgb4xVNsycA+33u6l7Vx25RbuvecB1rZmFuIaOh2STZf5QQjyE0W233g57U2mkfEDqjv+jAlCWiJNqqcbXa5RHxolLtcSblwH4R3IGkw9RLWmyGxciZPLoYdDbEUnUWeKyJmUFslzW7BQB7FEIFoEVmtM1TTIF9haGb18HLV6+Ywyb7/hNTzw3Z8T+CFeypsvO2nZEGle8Os+3d3tXHf9ZU3rBM/uwpQqSC9FfWCE0uPPUXluL3EpScOk4yREGQvaYGoBbmeW7KZ1KDdLvKeOLeuEVGkb31SANQexfFARFhwLocWOG0zeYCYMdtxia433nkIPjNDMLNcfv4oLLz6XsbHCQtIyIYFoXi0bi4aLLjuPVWtmPpCwxhA8uwuZ8hBSID0XawxCHZj+06RYiwlCvJVLaDltLVQUut+H2IJK3gubmKbRIUI6yQdpVmSj36KBmklIFY1+HAeTL6NH8k3123bj5biuIormvXo3EijNp2UYRqQzabbPvGhI6rywBz0yivDcxNKsITEEO/0/NiHDBCHemg4y61diRjRmNEgIEha0RUiFcAThxDC1gT2Y0Ec6HkJIMCT9HFxM4688QOqLCqD7h5vKvumck3nVljOZGJ+39VYkMDbXVonVFtiy9SxOPb35CU/w1LOgFEnoZ+ZiLDbWeD3tZJb2YIY0thJNZwIJqQ4mrFMb6icqFCCMqQ/1E0yOYsIAnMYy11gw5sgW3SjCdTAjeUy+2FT+a6+/jLixQpwH8hIYmGsrHWusNWy74TVN60QDg0T7h5AtaZoz2yjS4qRz2JKA2CBciVASpEX7VYL8MPXBfuJSGaEchOtgY00wNkx9eB/1/f2EhTGkoxDIAzPiSAVAG3TfUFMdLrr0XE4/8yQKk/NaEg9LYPdcWgghyOeLbNq8kVdfcHbTev4TzyRWNBWsmhULWEGwf4Ta4B78yWH8sWH84QH8kQHqg/sIJ8ax2iCVkxCjLQKR/B/HmGqNYHQUf3wIpE1cxbTbaV5EysEMjWHL1Rl1kFJy9XUXU6nMa0m8VwJPz6WFMYZazZ9e6laqIaPjZbQ+MHg8PkHY14/IpI9uQdYiABOF6EqZuFQgLheIK2V0pYYApHIaGcKU1R0IggiBaGQdUX4Cf3Q/YBDCafjiIxQhsUGE7nvR5g6ThRr5QkL4Va/fyoYT1lAu1+aaV+2RJNeUvjdbqy0Vq5x8ynFc8drzMRZ27Rlj155xBoYL0/WCJ57BhiFCzX7jWUiJcJxk2isn+e0omO10FALhekSlMrXhfRgTIaSahe910ftGoR5OE/t83xjP7x6nUKyTbc1w1Ru2UsiXZs6tZ8azwA8lMAJcB2wmOf8JmrUwxlCr1rlm+6V4novWGtPwX0Ynf+OJPPqFPtKtuXnniDaOsUGIjeaeJUrXRdfq1Ef2YUzQcCWmqR+WSkGlTrR7HwB6KpGxljBKAtl111/G8Sesnj65PgIeA95CchLxJ3nIi9uA00huk4wf2jIKI1rbssULL9l8B/CM6yjWr+li9coOVjXOqUIh/vW5oPb+8dDvl4AnJUqIo390CzaMsFGMbM3iLO9GtbclfnseBNsgoD48QBxWGzkxL0r9ZCNLi4jLFYK7YhNfCIx3dbSwfm0X69d0sbQrCxAvXdZ516o1K34c+GGzIX/GAQP9OuADqA3rDgtKeeCnwH82fq8HOiCZupVK9f7WXPZdZ519yt1SioF0yl3Vlkv3NA4ai9bzrtr56B9+O1wu3Vuxdo8SYkVaqd4jEWyjGIHF6W7HXduLt6YXtaIbZ2kntu5jiuXERcwBQjpYHRPXKwjPRbpprE7IlYAv4ucqIvhMTftvtxnv27lNp+2VrrtMwF9lWzxas6lJIcRXgbfsfmHft+792vd3OI58h+NMn+ka4L+AvwfuAJ457CMfQb4h4N9JTPwdwE6lJKmU99nvfPunBH4Qkmx+byI5LnkM+IIrJcdtPJnY9ws1E3+lz69sfrpSvC8jFc6hBBuD1TEq14K7ZhXO8h7QLvFgCZOvgJKo7s5G4AmZ2y6KRSgHtMWfGCGqF5COi4tDVUaP5G31jLqJ7sCwV3kpREsakltDTzf0Ph14D7Dzlz/7HXv7Bp7KZNJ/bHR+F8kMvwn4TTMJjnaFFKBMcvnua9barUKIHW1t2UP96f80iiOEINvZiYljPClxpWRfvXqTJ2R1Q7b1rS1SUTfJHVqhJCrXhczmsKEgLhSxxoA2WD9C5jxUroX06Seix/NEw+NJkJyDKxdKgTGEkxPYMMbksg+FHq/zYtcXCDBx4npiDY7zXMOYXuTs05kUjuNgrf0bko2uP8xm7NmQO4WQ5GDvSIgB4jCcJl8g8KRif1B7m2/0QytT6Ts7lbPKpDxkrgthXWwpBAzIqcWDwAYaPVZNCG5vRWYzmGoNXUym+VwgpEQAYbnAcGns8yinqhyH1q4OUs5hFBwpis4pbX1pbmRMKQVEcUQQhTjWMhn5/72jUjhtUrnfT+WWQiQwoY8VBivAWpMULNYa4sEi4e48erICUiBSKWzUsLQ5ZiKulFilGK5W/7S/kGdgfIynn3+efKFAJp1+SfSfi+XOgVRBpGPqcURvWycilcGRiq62dlBOIS1TbdrXWNvYo23mS5XEVCNMJUQuyeCsWIpszRIPjWLDCOZwW8cVkp1+6Z7xONyTaVhrHIU82bcLm0lx0hxy8mNKbmxiMo7Huu6VrO1cnmyAW5u4CkFbVA8vinWMkOKoiblwJLYeYYp1RMbFaV+GqVSJ9g0hm1yTOhgWyCrFcFAfe6Fefr8QgqiR3gnHoe7XeX7/ACdojXIWl46XhNzWJe0ct2Y9Szo6GA4rTB+KYdGSeqsVezOWtXFj6XtkckGXfHShhurJ4ayQyFwLqqs9ySAQTQOcBVJSEmjDzlrpr2NjSmmppr+ntRbXcYl0EmB52ZNrLUG1Tt2T7K8XEYdoHmMiLbzPtQj305jE1x65P0CCjQy2GoLWqO52VHcn0a5+4vHJGQOcJXEFEsEfKvl/Gg/Dn2Vkou7BQwoEjpzDMnsOWPSAZrRmdHcfuC4gDtsrUUgKIvpSWel+VzOrvVeMnXYP4e4J9GQN4YhkY2iGFVxCrMAVgj9XC1/s96t3ui8BeUfDoluudBxwFFG9jiNnVigyxh9V8qask3pYhhqtYDbJqw01puQnq6y2NKZahUNWblMW6wrBU9XivX31yjtblLPQCx7z42KxO6wVikS+j5Sy6RajYwWB1b8Z9eLbhRJIzay2JhEgXAVCoccLSc57SJRPN3LaHZXJT++qV27JKGf+J7AvN3LzfXup5ScRSiYX5mYoWIuNY4Z19RPDXvwZVwiknp17cJCoOESOTpBSCtkgMyUlLUpR1bryaGni7bvrlfe3yGNHLLxEbuGQa5szQgiBa2FUhO8TKdSKQL4LbZMj05mQ3FaimpHP9pdH784F4ZZsKr01LVWXBCajcGIsCr4/Evgf843uSws1dY3h/w+5c4FA4FrBEMG7TTo1sDxUd6rIYAQvyiKkBYWglhZP9avgioqJByaD+qdsFKw8Mdt2vCckT1eLL+SjcLBdeWSkoqKP6YX2RO5jLYBAIA34wnxirMVeWU2LnUIkp+GQEGukYCRj7i6m7HlGm4G0ULhS4gq5X8DDAh52hRxMSTW7veO/EI6p5U5BAI4VaGt/UvPEbyJH3JyOxdWZwFJPiXjMib5Us/rBLptCIliUu99/Afwfbc0Hg+WfUTEAAAAASUVORK5CYII=\" />\n   </svg>";
  var recursos_adicionais = "<svg \n    xmlns=\"http://www.w3.org/2000/svg\"\n    xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n    width=\"120px\" height=\"67px\">\n   <image  x=\"0px\" y=\"0px\" width=\"120px\" height=\"67px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAABrCAYAAAAhK09vAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wUCFgAlbUKP8gAAEjBJREFUeNrlnXmQHFd5wH/vve6Znpm9Vyt5dWKQLctIxtiST4FTNgZCRBJjgwFXDKQ4bEJIUYRwJFWEEK4KVSEURYC4UkVIHAjGxBjHdmxzWD6wjI1lSbaRjSxrday015zdM329/PFmpdEe0szs7M5a+aq6dnq65x2//r53fO97vULf8DXmXfwQMg68cwsczcOaJdDbAZUQwmg5lWAFnj/A4dHLGDp6Pm75W0j5U8T8F20msZALkLMU5nASYKu1DI2u5HdH3oIlX0vZO5OR3KsoeuCWwVYg5UtE0U/R7YJSCQDBvD4VDbiVj/GDh95FOXg1o/k0Y0VY2Q1reqASgaUgaU/+Yj0wv2U6KZSCCzqGOAI7CUJVa9FKEaD1Foazm1ESEhZ0pUCLYaTVQdrqID4hz9cCDlBuBxTl/8Hf8UjvajrRDHpZiCqgLANHAEK04ACE6CZpvxXbAimpqoGiN5VECTnlOTjAHcChdkCxnupYSiQEj3YPcnF+mLeOvsAFuYMQFEEmIJk2mjR3eeKEMwGEsaAc5kgk+2fQzguBX7cDirrk0vfTEfkk45g9mT5+3ruaF1O9iGSaHh3ijB2GRKr6dOckY8CHgbSBIsCPAzK2pttxCOOZ7v/vdkCxADQCiWapXyJC8nDnGdy/bC1XLsvwrh33c9ZzT0HoQ1e/gaObanN84Eng6mPfCJ3CjzSRDhFYU5TlnHYAAVDnXX5TzalAAE4U0FsuwZmv5PFLXg2iE6KYvvwYxDFYVrP5bQAuP3ZmINh0OxIpAjSPAncDXwb+BRhtB5RZaxdLSabsksqmuXfTRdxx2UVct+1Rrvr57eAVoaPbNMiNac2u6t8JYA+CQ4T6WWL9OEo8C/y2HRDqhgKghUBo6C1AYMFdF13KvmWr2LLjYda9sBtEGTKdRnvqkzuB1wMvAgeAto1FmoZyHA7YIXS5sPMVK9m95nrOOjjEDff/hK7hF6F3ab1gssC2dlf6VFJ3l6IFCA19BQNnxytX8fVr30d+ySqYONqK3mnRSMM1mYSzYhSOdqf5+js/dNqBaboWsYSBLBztck47MHOqwekKZs6lPx3BtKTkpxuYlpX6dALT9Hj9pGB6DJiPfv/bdI0ONTKOaV60NpPMOAbXrZ5z/GgXlAUHI4RxjrkuWLZxcSiL3561jsCSyAiiqHGXWcuhLBgYIaBUgliz65z1/HL9BkIBEovdKwcILRAh5EsmS9GAtswLlJOC6Rlo1vVwHEYYQrFAtqeX+y7azE8u2IAVgdRmYNntgapAFJvp2aKBMg3M9R/iw7d9m77hIaPq6c7GSjoJxHXBD7nnki3cvfl8xjMJBvKgYjPanpTJz800K/MKpRbMeKfDt667mcGD+7js+adZt+c3IG2w06dORErwXCiUGFmzmn+74gqeWXUGdgCDEyYP3cLZdqNQOoDNwEXV4yZgpB4wvQUoOAmOvvpstp93Ntc9uIKrfvYjIIS4Z/ZVBCGhkKPU0cPdF1/EIxvO42hngmV5kLFJu9UyOxQhEFL0VCGcD1wFrANeUXPXj4F/ryejWILjQ6pifDP3brqUnWvWcN5Le7ly5FGILbC7DIRJOEJCYZyRzmV8/o/fxujqFL05WDEBUYu145RQpBAbg4r/p5WSd11Hr7OSYNbfv71eKJMy6ZtRMewbXM7zK5bjv9DNq0Ze4Kz8YdOtJjvMzYVxxnuW8ZdvuJZCp8P6MajEBsh8ylQolwB/hhDvRmtZGJ+gd7A3EgI1S4fxRqAH4zxqCIzQ0FeEWMC9KzcSrNnIm4Z28bqXHqfHG4Oyz+jAaj75xus46CRZX4QoM78waqH0YZzJHwS2Tl5QtoWbL+QrpUgmUqojCmf8vQO8FfheM5lrDJxefxLOBn49cDZXv/gb+r3DfGPLmzmQSbI8p4k6Fs5vaQErgH8FltRekFISeBWnlMu5qa4+ZoEC8I5moUyFs8wDz0rw07UXc7AfCg6sKGoqC+zHlcBu4D+mXTFjiERxIksc4p9kSHE1RtvmLLGAVAjJCBSazoomaoNjWwIxcOtMF5Vt4eYKquyGvlSzppEE/rBVBbIi8JLgKzNCbYdMtuPbgRemXZQSv1xJurl8pOxZUhAgJddLZcZYcz1QUHRaH/fQiNT2PrcDf3VihQUCEsXxrNc32OcLQUJrkKoatYGZpPoV3hz6/lKEONp0SQSoSOMDpaSFHbdvQagWym3ToGBMqJTLW1FInO4iDipQ8XSUGx2xi9kCY8MH7gx9/wdr1m8sJZIOcRQ1BUREoIKI7MoOYstGNpHMfEB5HGNCa2tvsBNJ4jBmeN/+rFDhGSMHDuAWCrqYnfivMPC/G0fR/ziZDqRUSKXQTcyARaxRUUx2dSeF5SlURbfVfqYO3m4DPgUCpSwQUCmXKBXGM0Mv7cgEvj8K4i6p5C12IvmQZdvEUUQi6TRdABFrVBCTXZkhXwUi2tmgAJY8sVu5TaM/hYayV4iLhbHfeaXc7igOditpP+ykMztoYXTRYgQCYPm+V3v+lJTqlkJu5HAhN3K3Rg8pZR2wLIdW6/NiBQJguYWJYyda6yiZynwgCEz8nWUlJq/8vwECYInaJQitEUIgpUI06hU7TYBAC9d9ThcgCwrl5QJkwaC8nIDMGYrWEMfipCsWLzcg0IQ3X0lNtqTIlywcJ0KpmFLJYsTrIiWSRGHN+DyWZCih1yjyg/MCpAvYhPEjbwd+vqBQolhwJG8RBIJNa136+0pcvtpjw4YszzzTz5B6jiXdkjg6vgIoozL7MqvZOTBIxxyH7sJsBOnDRGJfAFyJcaSvqd6igc9gwk3nBiWuusRFzSNUUlOoJChVEkgRkrA0TkJzxboSF64tcf3rJ+jsDcAXIGHjGUfAv6268lfTlVvD3FG4kQdyg3Sq5plICV5FvydpiS/YNitmmXMK4EsYP/PNwOGmobiBDULjRxblQOJEGSayIet7h1mzPCSdSLH1vByZTMS5r3AhFYOrYCQxxQs0g9JFA2wWezhDvg5fC+wmbUcK8EM+NlrQK7rSIptJktEaO545uT8CLgM+IAR3NLNCKHbd+ilI+Dx7aAW/OrQCy0pwTs9+rjlnO91n9kHHKtAV458rWuZv3TmEIHw+mf84O4MelkrdrLZ0AUdijaM15XRSFLvSOErSEU1Zr7eUCQgXQBDylXyJr+qY0UbGokJvezegQVdXl7QGS4MHpFaaSIHQb64qCFAj/Lp8OZ8tXEOvBNVcQm8A7ps8iWIi2yLfnRY4Nj22jdAQxTHCK1MouIhCEWssq9NBwA1CzOxunU2s2D2+ljsJU1eA0Eek9RwDojXEnVxoPc9yFZONFenmTOji2hMlUUFI73hBFzrTYjzM44zncLN5bbllVKGEiGNS1e1GTzSa2QkNwbwMH7SNVuNstp/lh94GMs01uJunfqGMT7dzPK8PPbsHXfFZoiQohUgc9yfvoYl4/wUY0SokguucbXRJ8BtfAFaYHmWaWBZ4Hl06pjvtIJwkwj6xvX+0mRIvABQNcYY+dYi1VpYJ3XCmm4BlM10QgnK5TBzF2LM0pI8vUigANkiXtzs/o6ihrEUjbdVrZyy4gCBAuB6hmr31XqyaAqAh6mdT8ld8OrONMW169jrlduAbU78UEoIQ3yvToWauxQjw9EJDSQFvApbXnZVO8Zb0XVxojzEW160tR4E/B27A7Cs0qUnwPCphSDiL6ewCwrpymAOUDPB71QL+BPMU7gH+or6fa9ApAD6R+T4KKDZmRrdi5jz310BJhSHpGaAMAbc0AwROPSG8FBOmMTn5euUM92wFPlk3mLibPmsfN6e38Y/u60g3tnl+P3C1EHw2ivhbr0xGmMe6F6MZ2zDtyDOYLXkthXINQn0E372SODJT1NmdJudiwr+eqi9LATrN76fv4sHgXJ4O+hlocPgvBZ/zfR5zPS5QkgereRebhTAt/ZrPSeADmE3VtyPllbriBfhe3gTpnVTeUX+WczajSblHa74IPNRKIJNQNgIfB3YC38HYrdGOKNS6XIqQ8lSdxbWNZXuiGeUb642OyTwtOCAx7cFXgbOm5yoTVFxBHAanKMHZGOdPI1U6ZkYN9kbzLhIT9jnzNFgqtO/Z+OVKHSbUuLa0xozmBcrdwL0zXjUmlKjThN7WePatMaP5gOICP5z1DiFtKq6sw4TWYeYpDcrCm5GJaRMcsgQH7enHZJd8J+YFLtNjKqomJPxyhUQ6iZ5hkCgwLi/JR9C8t/FidoCM+ETfj7h57INUQkFingLeLA2HqxV/W34IS0dMbRomoWSB/2WmgD4hIAwsXS5VhNMREyMRmHcnKQmxNntIihXwgl6m+K7rF0kfT7JVP8735GYGWwxDAIEQ7EnAOt/loyNP8iej20FaTB3YTw3vmjnKUcgEvlvB0j6WbRNGkKu4uH4nWQ/ylYcoB9/AUnc1v5dHgH+IpcuHCAY3Q6V1QCQwogRCwDvy+/nK8Db63UOQ6JuxX6+FcidVz+y0FG2FjiqxODDukguT5L0MRd+mHNwDfAetf0zSgnQC4jmovU7gSadloaIC8IXgRRu2uAU+P/wAF5aGjLk4A7O+QagWShYzwbvmGAwLCECMC8SE6CZ/BGJdQPB9LHkLSesBUxltTGmRiMSYykHLfP7cyJO8f+QJOoM8JHrMHSd5pdKJcx/NbSiuwQJ8ECMgxsUhXHYBz2GrBxDsxLzqY9GJ1BBKwb40pAJ4+8RLvC+7k02FvabtSPZXYZxcFU+EIrmPkL1iTOwTE/wCl+1Ifoc9PfB4MckxGB3QHcCV+/eyNRjmxtxjZunG7uBU2jE7FMWIyIlN4gATKCBRVxpthRELwd7OKozDe9n60l5u/M3dcPYyGFwKgcZoRv0N1XTXgWACm0X5BpxaUQiOpCDyqzD27+XGnfcYM+nohWRXtRKNj5HnfWNlo6KFIBZGA44VMoZCCtzqK+dsAa4PS70CNz3xEO/97QOgbMj0AdKMm+awirV4oGhAxASOpOiAW9OZuUlYdyDPqqEhwoxDyo+4eOeTnLvvIJ3ZYejoO74xcy57nhcVFA3YGjp7uOoXuzhneIJk8ni8hZuEsw6O0nHoACST5v4oNuOi7oHqrvjWTQvaD2USSCaER1fS/3CZfrETVE0lYyCVhCUD1W3pNb+fhxdLtBdKLZCH++GxHtONWJ0zP/g4XpAOoH3D0GlA+qEzMNPYNgcKtgfKIgbSHiiLHMjCQ3kZAFlYKC8TILBQvc/iArK+erwFswx8DZBbWCjtAnJ8mrCxCuEqzPLu+Zw41X0P8PWFg9IuIFovIY4/ixCXYoJ+TtZMfGIqlPlrU9qnIZcBTzLufYQovhAhTlXHlcC75h9K+9qQm4AHsdQq8mWPfDmHXVfk7qfnF0p7gCjgm8A/Y1wtoLXDmBuh9akW8cC0O8deSN5aKLVAHlkwIKuAX2A2KRwXWwly5SSFiotVVzX/en6gSG08Qo/0w2N9CwFkK7AD2DLtihAQ6xRjboQgrGMieQXVMJTWQrG02eGxuwuceCHakNcAvbOXR0pyXoKS79W5BPOZ1kNJxrA/AxUFiSZe2ti4fBUTPTmzSAF+lGbMDVB1bQq4FjizxZoSQ8mCilyoCUQF+KeTl0lJJjwbLyidtG2xFDhJSCa+3LqiWxpyNhx0IL0gWjIp38SEk8wsSkAlTDHuVVA1MTaWZbx5yUREwo6jsl8oHjrK6AtDy1o3ohUaQmk2SqkFndBkMTGzH50djLQYcxOc0V0m7SQJQxEWvaxX8pLlXLFUzBe7/ZLXWS6639Wx/ljroDgR7E8Z80k1Feg8F/mHk0KxFEQiURkazU6EbskbKzgV1xPlkieI9VLQSKX+xkrYX4BWzX0UUJaws8P4UGvedLhAcgD4T6YM1wETxK9UZaKYLR7YfbQzCIOUFBJpKWGbjUEjwI2Y4AKgFc2hxryDbCIJEwnzuT3+kS9N+0YqECJ/eOJoft/EkW6tRNpJOcJ2EkJZCkyE9gW1QFoDRVbHIs/3QWi17z1kJg74/mNnlq2jKMztHT2oDxfGByypLCWP/ReuEvA1zF6DA1MTmrv5SA1lG0ZSkFzwtmSq/D1CvAFpUSqXxP7xI91e5JO07BJmh/vTVXDPYOL5Z5S5Q1HVrjiStP8lBuKXGrF9rDD+mkO50Z/FgmcdZd+n4TlgX72pzB2KFUM+CRXLtCdtZQJax7fmyqUdQRzdlbQTz2h4vtFk/g8RJeO5oYxHDgAAAABJRU5ErkJggg==\" />\n   </svg>";
  var aplicativo_mobile = "<svg \n    xmlns=\"http://www.w3.org/2000/svg\"\n    xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n    width=\"120px\" height=\"67px\">\n   <image  x=\"0px\" y=\"0px\" width=\"120px\" height=\"67px\"  xlink:href=\"data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFsAAABtCAYAAADOFR0pAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wUCFhABG4N5cgAAHplJREFUeNrdnXmQbNdd3z+/c869t9fZ3q6npyfpPVlCgGWL8kIBxlBAiMvYhuCEGAdDWFJ2BQguUpAEUoEEYhKIWQoSkrCHQACTIo7BQJUNxhK2LBCSkGTty9N7mrfO1tPddznnlz9u90xPT89Mz9J6cn5VXdPT99yzfM/v/rbzO+fKz/z3P+YVRLcC7wC+Cmhe784M0UPA/wH2DJi73iMYoB8Afgho7KcSa/tD0g2/iwghBLz3iMheqv5S4H3AHwLfCVzYbQWvFLB/Cnj/firog3n1ykUKX2DEbLiuGmhOzVCp1AjB76eptwD3AW8Azu/mxlcC2F/PPoGGEmxUWV5eJM8yjLUDV5Wi8MRJlXq9uV+wAU4Cvwd88W5uut5gC/CTwz+qlgCpjl9RCB5EOHHDTQRfft/QkAjWOdI03X0nhVGi543A2yjl+Fh0vcF+PaVSXAOk080ofMBZgzVmF1WVM2Osw9poZAkfPGignOMxSCAExftACIFaNcFag65zwTfxOQT2XYP/LK20OXpoirvuvIlTJw9hzJ4U2YGRAEGV5ZUOn33qAo88foE4tiRx1Af89t3Ud73BTqDk6KXlNrfdeoy3f80XUalE+633QOnE0RluP3OCm08d4Y8+9iAiQhxZVKntpp7dPKeTIA+w2k45eniKv/eW17/igB6ku+68iTe98Q7a7ayURruk6w02qkqWF7z+tWdw7rp3Z0d6w91nOHKoQZrnMGzM70DXfXTeBxq1hJPHZ653V8YiawwnT8yRZsWu773uYAdVosgSx69c8TFMceR2ZZb2ye3hnl3ROPaEKoPm1Cue9tpXV4TJmVcikHqwBmpWCa9gPJ/EcwWlDtyBJd6WTYRdimsA3Jcf60xsALFRFjPLQwsxy4Vhygb2oMRfFvoWVvgUKRbLE8xyK3b/lQ6RO1ndvaAfl4LCrU3lC+IVfuelKRZ8QsOGPfDE5KmDAopHJ9Y/1/GTEyNBocgtp13Kl811+J0rdSqa4WToIRzswnVSIhUEEAwyrjO/a5q4NWKApdTweYeEN58oWM034zmM9UF+NtE4BXcos9fJeFlMPwUyL7x2LueGWqBdDHd3cqy968kY+H3LsntE+2UBW4A0CCLClx7NiETxul2PD5q/DxzuPfHEy8PZRQ4h0PbC8YrnTDNntbi+Eb2JjXWbz0SjfqqKqs655vRXaJz4UPi/6Aa9ensz45nVeAN3TxL6SetfI9Dxgg8Q2a0bcW6CMWMRGlP12j2VxtE7vPfMZPmFVpufOxK3PnBrI+ORxcQmwzdNxBqRNcTlgE07AVZyoeGUuw5lzMUFnXx0C87agzfegXJlI47fWa9W7whFAaokSXxDksT/PmvL7Wc6q9/25HK07Hum56CU3O1gGXFvaciV0H6vtHgEzxEM/0XrTGNGQy4jKhpxXQb+XcoNh5LA2062mUmUVhrwbvQap0uzfCJg50VBNUk+zxohL0q/0RcFxhiq9elvPZnmv3/oWnbuQr4/STZoFQu65qHqACi/ISkLpEDEB7XO9ACeo57rrSZ+uGzbI0cqgbef6lI3gYXUUnjFh9Gq0Jm95VDsSNYYROSIBmUwRBZCwCPMzTR+/sb66oeeXlad3rQ8Ow7IJX2EjKsScMDbNaEOm0ICxzEsYKliDsQiMAJtD8Zr+tXHOiemHSevduVxJaxs92S6CWFdoqckvZDehmveF7hKdGq2Uf3HhjQDkvUSW3R3cPE3aI+jlX8iLc5LClie1TnqOCYk+NeolUPVwtvPuFtumm0+mHk5Mpfw3Gq3+9sr7fa/UNWRiUAuhMmEhoIPqIaVLe3ZInB2xk7NJhLysF5kq+jlRtmsvfLKYTGcx+AwmH4KBLqhrA7KlB1t7OFym3sSW/iGM44vuCGaKjLBiWJEbk7i+AdF5MarS8v/yIjZJHdc4ScDdu4DQfXSVk9OEZTZinBDQ8wzq9tUZAzkBflP/w/0hZfg2GGi73s3VJIeh2+ejG0h7E/qZgw3wLw2n0P35gJ3HTYcTyx5N6yFjb2CMYZmtfruNM9/RZGPDffJzTb3lVq3zaCUyNr2VpPpA7iKcLxueGI7SSflKMNfPUp4+EnkzCnwfrKG+VZjQggepk0GYvC6sRshBMQYmpXky4ss/5gY6VlFiiI4OyGhrWXjrRACxpjRqxsBjlRK+ed3eKpldgo5OofM7UWdHhD1zPXgPWGL6KBxBi3yqLN4jcgaCgSPYFBcZw/pWOOQqmKsuVoLcQnOKLALZToWKq70vsr7RlU2XPfm3wdFsm5969ZiZLjMCDEiocdExpVjMoY1A6DMUQOxiPfTpsgo4gZd48iDEocCl/t9JxmOpKCKVb0AIKXbPhLEmoPYMKDUxkB7U6CIof91i7KA6ztxAkXoATXuqBRvBJN2sQuX8VOHEWN6zAT4HPIMvfRSluIobIRRMAQEcJ0JOTVBFWvMOd3oXwyVgdhCPWIkZx+stDCIKly5BkWv4elmaTTrqMkcbY04ERYKQ2PpKtYHgukFQwoPqmicwKGjX1mJKr+gvriv3e3+vg9hOQJcs1qdDNgoVuSiwKpukeCugBOobCGzB8WF7vC7MvrxX/9ewWSr2B/5FTh3keLOM9gfeV8Jeu4317+FNZIAD3XqLKRNvrCZkaaAcVCtQa1BSCpUjX11XcOrUX1vu1r54cuLy98Y8vQBF/aSRzUGqUIw5CXuOlIYqwpGwJnRwiMaMOwKtnlExqWgcHUR5q/CscN7rqYb4NfnZ/i2eu4/b86YvNIUjZOeQPdoCPSFc7NSudVM89GXrl69zbW7E1KQKEbkpNZrdbF2iyUqRdAt3dgCxQP9u/ftagtQrUC9CtVkz9VUDLzumHD4+CHxFSnjA8VodsjznHoSH63U6+93tWgyIW2vShK5LzFGrOpo+VeSDnVPcb1f3mVbPIij4Tp8BOUwQrdXhg1/ezUYoB8jD2GozOZ2t7RbRtYDiiENcPd0yvHbLCTBdAs29X/4v0KE2ST6ThcmlIlU+IJGJXmrM5ZiO4snlE/3IHz9zn9GPE8TQDye7URIacuo6aewyQaQxqcR9WhYDyVYgYVrHO1eJpcYn48h1pzDZxnJ0rUjblJZSiK2Uomit6gOBD5GYq0UOiAiBooeQngaQ33IgVizldfKxsR5RvLBX4YXL+NPn8C875ugEqH51hO92V5PqPic5s/8Gjx/EX/8EOZ73gW1SmltALJ0lbzTgWod2c5aNwaMhW4HWbiC+sJNREH6EEii6Gtj5xoh6JZYi5SedzbAtuPM/WaL3GKD4u57BD77HPqFt5WPy65tR4vVnPj+R+Hhp9EzN5Yg9+1oKDu8lWfUd3REIC+gvQitZSICy0QPuYPO/5Ee2NXIvcMZYTunSYBClW4AMyrsN9L8GJQ567CrgM5NwdFDMDu15iSNToKUnqk4AJoCUt4VZqfg6BzMTa9rlF5RiStg3fpE9j3Hvu2ZdqHTgc4qFCmRtRQ25tpq9l4nByxGVBUrhmocvWWnRDOR0r9I/fXOXd4ppaH3PUCYOYx1FtIuwbpSN/gC8hSyFLIM8QVGAOtoqXm2lfODAf7SZTtFgHZJhQYqzr4pce5ICDuD3S0gD2uLDWPlPeuIh2BPZcachjVr33uKRoOOqTF7/mHypIkPioRioDFFjWXVxfes5PrBTuH/OHbSilDcQUdG8qDMRPFbnBGynWLlCqu5koe1f7eMkGwV7RgnzWZHtHX7Mv34NkGpCtwbjjKzXOO1s6lKABVTyhHrkGoNrTZQ42y+svIcWdYqRFDrcDV3gAEIhYoxNJLorUFlZ9YRWMqg65lAgu5Y3R2J9XZRQwNcywwfWTiKnQvZa6Y7pvBEwVqo1MrYCNCAN1Znpu9/aWn5G9tp9qHIRrjiAG0/VcWInE2s+fxyy/LWdfcTGBYzyIOs65iBQU52JXEP9Zsy/nA26XDLrZ6zx+pJ4WqlSd8PI/tSpBRAZC2H6tVf6eb5R5Ww6qw/uPzswgeq1cpXO2PRHZwKI6C5cqXbzyJSNsZQRvFbb4p00NIYHTFR1Z4O2Gba1owR3VlFiqDW4J95ktcU57A3RuS+oNjGsSl8QTVJmjVj3rO8vPwLBmM5iI8YixpDHLk3WyM7co0zsJSLzneE2AyMbODTn65AbzKCbnI3+w/meplejHqresJgmd5Fr0NthfJLGGjAOeTSS9inH6RQoZtbfNjOgxTURtBpU++ufktFwGUH5NMEVQRjKlH0VTt5jT1GYSWnSD02ifqW30YubEn5f0cU7XRhtQ2d7oYyK70ybRRtd2G1s15GRpTpdKHVgU7axwSRgbYYbmttgHDxBUBR59h216mxpXPTWkYXLmMChyKX4KID0o9BwIj5ksiYuaA6lkBMPf3zE8yaFAG6vcf6v6YVlkSwGlH73vfQbXfRSozGjm6aE1B+Na2yIoILlur3fxvddorWK6g1dNMcRfnNbpWWCE6nqfzAt9PtZGizigprZfptOY2o/7P3kK6WbRG70jbNcuiugo22ybcwYG1pb68sQWsJawwp9uPL3Q6uOJC8EaHwnpl69MbIGnwY7+SDIhCCEvp7r/pOWH/t6E1FPyLpaL/mVeT9WtsZeV6eiPPmfhlxtF97+1oZWU3Ji7LMVwyWufuOkWXeNFjmrleR0XO02tmayy7GrnPE2tB7LjoCRQ7LJcgUOc4aUrGsBPnP1toDTD8TcMa+0UgZ6N+RtRWCite1iKoSeuzd71FrsI6WX58+kbWMo5YMCPDVdENu5H7L6EBfNYkxU3PI4mXU2gGvKUC3ve6i5ynGGGzkyDEsqXmfRx9wYg+Gs0uTD6lG9stCCGOJkHK9NYjXoYjecLGRvHBARuEAk4qM3t3YP2gGhTB3Annkfmi3IU4gz0s3Pc/A+x6TGwoxulDwu5nITyXO3Oe0IKAHw9kBMEZeHVlzJGy7ULCGMwCrncyEEMtaXtxg/F6gm0I3UyaRQq5ANRHiqNR9qx2lGIjRqJZ6rlYpfQCyFD93mHD6TpoPfRySmDS49UCVMRDHXJPo/mtFeGeh4bmalHvPvCnXmlzF7j8EFFSJnf1iI/1U8x3ANmUIstVJcaayNrjhlfXCl4AfQBc3g62QRD2O1lL/ZTlYWb9uXRnKBtAQkDTH3v0GHl4s0Ccf5Oy0wRkILioXe6s1qiI3HkrzH7rcWv3xlcI/01aD6aVPuFa6/1SGzHuOTdXPRNaQFTtHWwQhyz2tTr7ptJw+4GXuXA+QCYEtAzk2kespzQGBbjbEEATxnlginq4cJ4/nue2IRSylhWIMEgJV9HijXvn22PC2cwsrrw4a5vuVusYBHKaSe0Pk7BvG9fyNCJ0isNLJcZX1nQeqYd079JDEUIm3eE72Kf56KYTlAoxAvTq6vr4vJYCPK7QvX+Uriyeo3WLwLqLom1DFuptehIKpauXIXF786OXF1nfZPmfvV2YrEFkribWvGjf9WATSvKCdZZiabKhrcAm38KWzNxElqaVJ3A/t5gVDqcXlV2vLL2pjaLeIHv0ULmvh4zphVFRTy4rzADNp+5tq2vkeCbYL4Bba3XG6tiV5VSJjbjhSrx0aXBzdjoxAq52RF4ENynEAben5Bp3O8ON8MKRaZjQkSTmhnS7kAyfUaSgz1RqNXjKltUTzzyKtRYpac+sF5d7s+CuXcKtLjdi5GPUl2NV4f6kMQRVnzGsFdeXa1Bhoi9DqpmSFX5PZZeBoICzb8/j7JtkkqGfer08wA21t2MxU5ojL6mIJ5qgx9l30LIPFK0TtFdomuryShk5f6bqpfZ5g02v25l2FRK2h1c1Jc09VRouR0FNa/fXTvXRqJzKmF+LQUj/EQ/etrR710Te2BLuf3ye96VEtbe72Cqws43yGiROutot/vZTledTbouIutveXEZUVgWONylTiLPkudjG0ujk+BDZKkfVgZ2ktCNud0XUQHN9X6taODtaWPk1AY0twVdzSNXT6SHnFKxQppB3odDBFhliLjxIudIofW8ryX7Qi9HNzXJ7vL56dFgVo5ZhFyMcQIcYI5J7VToYVtza8cjfwujViBNIMslz2vpVsO1JIYiWKtPS4M6Hwsu5J9kzPalIyhOYF+fHTuIfuwS08RqhPl8eQBl+mxxlDESdhVe1vLhT6M500/6vhtDrXiPenfRIjxIZbgm6/MrMGtgi+KGh1U6wZEGFDCpLegH3YuFHswDz1ASdKKdsJgwpyIAqJUq6mT83R+bJ3EN3zYRqdBSSKyG1MB7vQRX4jiPnFDPNooQWxkdIsHATb7ddjsGBEDo+bxSZAVnhanQybbM6S6z/Kfc6K3b5NaraaISPrMjuyYIfyOvoyu99z014luelGPvXCl3Phgb/m5qYsn8nOf6dLKveumvjFqioWxcDI45mc29XhsqPJiEzrFmnBowaYZQWrnQJTGVCO/dt73BQAaxSXHEwIeCvqW3CR042RqAHlOLg0FmWeTqE8HA7xbKg9fz6Y33mdX+Co7eLF0dKtDY59gy2IMUJl3MwqY4RWmpN7T9xj2XUra3gP43iW5M40upJN+YO64y2llSSBKclo4tyz9gbm/WFuz65xV7TAlElpYxgVtHB2v4+oYAUZ3340pdmXFYGkt1Yp0ltdDwcF7jbdHd7vuBtSyqjVGpMoDbpkWO4vjvOEn+Pz3VVul3liTSmGDrBx+7afTOnMEsYTI4jQ6mSkecG0gBqh3c1YaXWZmW6QZsXBOzGDAO9jMsUIWeZZWFzF9UKRihARmJEuXXXclx9jXiqcMpcQ1d7WpR7Y0T7FiNEgIphhEbB1j5VWNyPvB9utodvNeOr5i9x6+miPw/UgtOIAwnsAdrgKVer1hAsXFzj/0jWSIWdQESriSfBc0ClestObUord/KWLI5tytVq57WzbXDnFIHJiul4dY82gtLF7lkjppZdDqtUSHnvyRU4em+UL7jjFcqvTW2M8OLy3h3LU2DaWnmpWSbOce+5/nCwvqNcSGMoH7d9TkdG+i6vc/KrNDWlg5dzzqAjGuS2fPV9umT4tyNS4Qwx5YLWbY41BgyIKzllC4fmzv3yUVrvLHWdPUqnE7JSYOR5s+yDpx7iFF+evce/9T3BhfqEPNMCuPEJ39iu+duSF1YsXmP/EnyAaEDs6WNXb2Ho32k943r4xg9DNPSudDOccIW3HErwBIUkisqzgk/c9zpPPztOsVzDlmSUjFiY3f+0rWue2Dqx579FdJP+LCN4Hsrzg4uUlOt1sEGg4qFen1I/dQP3ms8w/cB+VmdmR4818YLYS3xE7YZzTHYyBPPWsphnWWYrVRee6raC1Wcg7RJEliixXrq1w8fLSrsWIMYY0TUe8zaM8/yOOK72UvN0JcQUqcUSjXhm+908PBGyA2Ttfw4sPP0CnmyJuyK1XyEPA2cpXW8ptdON0W9Eyp9laQpbB5eextx7GFxbpcV0l2X0kMkkqXDj/PE8/9ThRFG04XEWBLEs5ffoMp28+S5ruLYY/BPQq8MsHBnZSb9A4doKVC+eIXXOD7A7losGxurOvK7bZN7OhswGsCLG1rIYCogrppXOm2pgJeuw2o2kbQsFeJLG1ljTt0motU61ufu9Dp92m2+kgUoqmAziv+z3AtQMDG+DMm7+Wv/lfv0qe56Wy7FEWlNlK9I6KM86PKQd98NQqjsNTFeYXWlTjCkUwdJ79W4l9CBy9RYgS0SJHd/mWpE6acvTETdQaM5THBg1c7GWz1hpN0iIvbd8N3k3YzfR2ge8GPrSrDo4DdlRv0DhylNaFF7HNqV7PBQM0Y/duizAyx3tE731QksjyquMzPPTcZXwIGBfhi0K6zz4sZvFysIdPqa03c7HR6FzgLUi9pxpZpo4dXU+sGeyOCEVRUBTZ2o5iEaTwIQ6YcfTDc8DHgP8IfHa3QAPIOI/T0rnnePyP/jdRtbS9Cw3Extx5Zqb5iDWG3WxcjWyZG/c/P/FZHjt/jcPN3kEFqmiRlq+lqjYycXEP7Mn474LQzXI5MRN3ard84VueTuvdKvnmYr2FdeApYHk/bY61ADl18jRSq9NNU2wU0/WeQ83k/RVnSIvdReWyXKlXLW973S20s5znLy3TqEZUIodECapK3mnHaGsiIPcpBKXwnhOnTv6Q2Owe1cbEj0Iai7OD9zz6kd9n+cKLmEoVI3LkjkPNFyvOxnvZJqKq1Ksx7W7OXzx2gcfOX2Wpk012pEPt+9xz9vTRx+647YY7szTn8XCcJa0SMZnDbmBMzjbWMnvTaa489RguSbihXvk39cjG6R5PThOB1U5KJXb8nbtP87ozR5hfbNNK87WMqkkxmQKRCK0ikFXr78yLgkRSGtLlSmgQi5+Q4Noe7LcCD9DzkpLmDDapEYvcdKSavNePsbtgO+on6pAXTNdj5qark4+v9hsWeP5a5z3PLKeP4D1GhLpkRPQP2ppMP7YC+0eBHwauAj8G/PThW2/T+vQMc6b4+ZqzsleuHkVp1t/APlkyIkRGeHZp9acutbNfn7al/nMIN8gK52SWLhFuQmCPiq/eSPkuXYBDwH8CPuPhrtnp5tcc0vytud+4Qehz4SMIkQjnW50PXW6n3x9ZsyGk4zHohDXkKAX528A/GP5RAX/5pa5/6NOV/Mo8EiVIkgwsP78SqQTPiOBEuNjpfnS+3f272tu32IfWEUhxfCY/RYrDTehtOsNgvxb4623vUCV/8m/Jn3wYXVlEqjVwIzb19BP0wvbHskyWtATaGC6upn94udv9OoOEPJQnSF5vsO9lzJcAa7dN/viD5M88BnmKVBvrhwoaW2YJGQNJtdzY87LDDM6U75053+r81rU0f1fiLCEEMn99wB6U2W8bF2gAqdSI7/piqm/+OuzJW9FOG+2srmfUXJqHF1+A1Ra4uDcRLx/Yca8fzy6tfmC+lb4rtva6PV9rmA1w9jPALXutqDj3DPln/4aweBVay8jyUq8FgZlDMHcInCuTxido4pViQ+gWvvXk0uo/XU7zX5uKI2Jny5N7riNn902/794P0ADu1K24k6fJH3+Y/M//L7p4FZmZK0XK1UtlhufcEWg0yxsGj/85qMEYAyhX29k98+3ud6wW/rORMdedo/skqjoFvABMH1SlurxI/umPkT/4aci6SHOmDGYHhcYUzMxBrU6ZCbp/0J0Ixgid3K+cb6c/3sn9B8RA7pU8KDVniV4BnC2q+pvAuyZReTj/LNk9f4p/6pHy4JNao1SWxkK9CVMzUKuV8tz73R0P1zPnrEDHBy51sl9aTPN/B/KcM2XST6cIFK8gsB1wZCI1A+bkLVT+/ndR/O395J/6GOHC81BrIi6C5UVorZQcXm+WoEcDxz9rGAm+MYYyFV3pFEW6kBa/dS0tfi71/q8Ta4jHOBHiepGoagT8OPD9k2xIs4ziM39Ofv8n0MVryNRMudGwt8uKOIZKFSq1chuAi0qF2ovq216GaJoXtIrwYCfo767m/ncX0uKJijNYKRcI1g5ffwVy9qA18k7gZ4HjEwV9eYHsk39C8fB9kGelPB8WI9ZCFIF1GGNwIqTGcM3z4S72v63Y6MPWRURGaOUFkSlf8mM+h8AGOEoZC/nmSQIO4J9/iuyePyE88xi4CKkPLCj3DmmJ1OPFcNXEH70i0b/NVe9tVipklSrGOgyw8jkE9nDU7xLwbuAPKEXL2UmBbU+fpXr6LMWDny7l+fw5qDeRpIoQcEZomerVBZv8qyU1v6iqVAhM6iVGLwdtt1JTA94PfB8wN8lOaNolv+/jFPd/ElpLiItYFPtXT1P5+qbhnAFSMUiRUY0isqkZonoTo4GV3H/OcPY4y2Ine6B/F1uc8H5goC9cIf/4h1lcWfm1ztGT39rtZkwfPYapNwjGkl28gMk6rLTbdK9cxlrLUjelPjUFIv9fgN2nW4D3At8CHJsU4AEIQX/PGfmXwJOjyhRpl87leToL17j8zFO0L71E8BkSFEuZRx3V63R8+XKi+hDY6RDYEYEujs/kN5HJKwPsPh2jVKDfDNw9KdApw1Y/C/wE8NJ2BVuX5imylGtPPoYUOYiw+MIztNMMj1Bzlti5Hth+C7AjPtk9RW5ipmturAzalwPsQfoa4B/2/t4wCcSBK8B/AD7ILlJ0Vy/P44sCxGBE1g6I6e/TGcwAKl9tKDw+3+FTDzzDlasrTDerOGcONHyzX7D7dBh4M/ANlGHamw+ui2v0MOW66B9MoO416qYZn/mbZ/mzex8jjh31WnJgXH5QYA/SFPA64KuA11Ou/sweYP3fAfzSQXd6mF586Rp/9PGHuHxlmWajehCJmBMBe5huBO4AvohyEk5T2u8ze6zvnwM/OelOAzz9/CV+4/fuYXa6NvL9jrullwPsUXQTcKr3OUtp6RyntOfnKJ+OJqWtPzjKxd49k81NG6CP3/son/jUE8xO1/Zd10RfBb4NvdD7QAnyCUqQj7IO+iwl4A2gAsSU2aMvG9AAszMNijHOvRqH/h88SvoOzbEfWAAAAABJRU5ErkJggg==\" />\n   </svg>";
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "box-solucoes pt-3 pt-md-5 pb-3 pb-md-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Container"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    className: "Col text-center text-primary mb-3 mb-md-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "pt-3 pt-md-5 mb-3 mb-md-5 title-vhsys",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, "Veja outras solu\xE7\xF5es do sistema de gest\xE3o VHSYS")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], {
    className: "box-solucoes--info flex-column flex-md-row m-0 pb-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-flex justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default.a, {
    src: integracao_contabil,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-primary text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, "Integra\xE7\xE3o Cont\xE1bil"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }, "Integre seu financeiro com o seu contador. Ele pode extrair informa\xE7\xF5es direto do seu sistema.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-flex justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default.a, {
    src: recursos_adicionais,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-primary text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }, "Recursos Adicionais"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }, "Uma loja de aplicativos integrada para voc\xEA ter ferramentas de gest\xE3o exclusivas no seu neg\xF3cio.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "d-flex justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(svg_inline_react__WEBPACK_IMPORTED_MODULE_2___default.a, {
    src: aplicativo_mobile,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "text-primary text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: this
  }, "Aplicativo Mobile"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  }, "Tenha o controle da sua empresa na palma de suas m\xE3os. Aplicativo dispon\xEDvel para Andoid e iOS.")))));
};

/* harmony default export */ __webpack_exports__["default"] = (Solucoes);

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/create.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/create.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/create */ "core-js/library/fn/object/create");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-property */ "core-js/library/fn/object/define-property");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/get-prototype-of.js":
/*!********************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/get-prototype-of.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/get-prototype-of */ "core-js/library/fn/object/get-prototype-of");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/set-prototype-of.js":
/*!********************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/set-prototype-of.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/set-prototype-of */ "core-js/library/fn/object/set-prototype-of");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/symbol.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/symbol.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/symbol */ "core-js/library/fn/symbol");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/symbol/iterator.js":
/*!************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/symbol/iterator.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/symbol/iterator */ "core-js/library/fn/symbol/iterator");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/assertThisInitialized.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/assertThisInitialized.js ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _assertThisInitialized; });
function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _classCallCheck; });
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js":
/*!************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _createClass; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/define-property */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);


function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;

    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _getPrototypeOf; });
/* harmony import */ var _core_js_object_get_prototype_of__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/get-prototype-of */ "./node_modules/@babel/runtime-corejs2/core-js/object/get-prototype-of.js");
/* harmony import */ var _core_js_object_get_prototype_of__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_get_prototype_of__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _core_js_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core-js/object/set-prototype-of */ "./node_modules/@babel/runtime-corejs2/core-js/object/set-prototype-of.js");
/* harmony import */ var _core_js_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_1__);


function _getPrototypeOf(o) {
  _getPrototypeOf = _core_js_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_1___default.a ? _core_js_object_get_prototype_of__WEBPACK_IMPORTED_MODULE_0___default.a : function _getPrototypeOf(o) {
    return o.__proto__ || _core_js_object_get_prototype_of__WEBPACK_IMPORTED_MODULE_0___default()(o);
  };
  return _getPrototypeOf(o);
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _inherits; });
/* harmony import */ var _core_js_object_create__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/create */ "./node_modules/@babel/runtime-corejs2/core-js/object/create.js");
/* harmony import */ var _core_js_object_create__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_create__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _setPrototypeOf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./setPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/setPrototypeOf.js");


function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = _core_js_object_create__WEBPACK_IMPORTED_MODULE_0___default()(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object(_setPrototypeOf__WEBPACK_IMPORTED_MODULE_1__["default"])(subClass, superClass);
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _possibleConstructorReturn; });
/* harmony import */ var _helpers_esm_typeof__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../helpers/esm/typeof */ "./node_modules/@babel/runtime-corejs2/helpers/esm/typeof.js");
/* harmony import */ var _assertThisInitialized__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./assertThisInitialized */ "./node_modules/@babel/runtime-corejs2/helpers/esm/assertThisInitialized.js");


function _possibleConstructorReturn(self, call) {
  if (call && (Object(_helpers_esm_typeof__WEBPACK_IMPORTED_MODULE_0__["default"])(call) === "object" || typeof call === "function")) {
    return call;
  }

  return Object(_assertThisInitialized__WEBPACK_IMPORTED_MODULE_1__["default"])(self);
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/setPrototypeOf.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/setPrototypeOf.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _setPrototypeOf; });
/* harmony import */ var _core_js_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/set-prototype-of */ "./node_modules/@babel/runtime-corejs2/core-js/object/set-prototype-of.js");
/* harmony import */ var _core_js_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_0__);

function _setPrototypeOf(o, p) {
  _setPrototypeOf = _core_js_object_set_prototype_of__WEBPACK_IMPORTED_MODULE_0___default.a || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/typeof.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/typeof.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _typeof; });
/* harmony import */ var _core_js_symbol_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/symbol/iterator */ "./node_modules/@babel/runtime-corejs2/core-js/symbol/iterator.js");
/* harmony import */ var _core_js_symbol_iterator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_symbol_iterator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _core_js_symbol__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core-js/symbol */ "./node_modules/@babel/runtime-corejs2/core-js/symbol.js");
/* harmony import */ var _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_core_js_symbol__WEBPACK_IMPORTED_MODULE_1__);



function _typeof2(obj) { if (typeof _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default.a === "function" && typeof _core_js_symbol_iterator__WEBPACK_IMPORTED_MODULE_0___default.a === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default.a === "function" && obj.constructor === _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default.a && obj !== _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default.a.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default.a === "function" && _typeof2(_core_js_symbol_iterator__WEBPACK_IMPORTED_MODULE_0___default.a) === "symbol") {
    _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default.a === "function" && obj.constructor === _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default.a && obj !== _core_js_symbol__WEBPACK_IMPORTED_MODULE_1___default.a.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
/* harmony import */ var _components_Destaque__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Destaque */ "./components/Destaque.js");
/* harmony import */ var _components_Modulos__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Modulos */ "./components/Modulos.js");
/* harmony import */ var _components_experimente__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/experimente */ "./components/experimente.js");
/* harmony import */ var _components_solucoes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/solucoes */ "./components/solucoes.js");
/* harmony import */ var _components_experimente_bg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/experimente-bg */ "./components/experimente-bg.js");
/* harmony import */ var _components_planos__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/planos */ "./components/planos.js");
/* harmony import */ var _components_faq__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/faq */ "./components/faq.js");
/* harmony import */ var _assets_css_App_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../assets/css/App.css */ "./assets/css/App.css");
/* harmony import */ var _assets_css_App_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_assets_css_App_css__WEBPACK_IMPORTED_MODULE_9__);
var _jsxFileName = "/Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/pages/index.js";











var Index = function Index() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Destaque__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Modulos__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_experimente__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_solucoes__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_experimente_bg__WEBPACK_IMPORTED_MODULE_6__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_planos__WEBPACK_IMPORTED_MODULE_7__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_faq__WEBPACK_IMPORTED_MODULE_8__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/fabiodiogo/Desktop/test-vhsys/teste-vhsys/pages/index.js */"./pages/index.js");


/***/ }),

/***/ "core-js/library/fn/object/create":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/create" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/create");

/***/ }),

/***/ "core-js/library/fn/object/define-property":
/*!************************************************************!*\
  !*** external "core-js/library/fn/object/define-property" ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "core-js/library/fn/object/get-prototype-of":
/*!*************************************************************!*\
  !*** external "core-js/library/fn/object/get-prototype-of" ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-prototype-of");

/***/ }),

/***/ "core-js/library/fn/object/set-prototype-of":
/*!*************************************************************!*\
  !*** external "core-js/library/fn/object/set-prototype-of" ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/set-prototype-of");

/***/ }),

/***/ "core-js/library/fn/symbol":
/*!********************************************!*\
  !*** external "core-js/library/fn/symbol" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/symbol");

/***/ }),

/***/ "core-js/library/fn/symbol/iterator":
/*!*****************************************************!*\
  !*** external "core-js/library/fn/symbol/iterator" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/symbol/iterator");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-accessible-accordion":
/*!*********************************************!*\
  !*** external "react-accessible-accordion" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-accessible-accordion");

/***/ }),

/***/ "reactstrap":
/*!*****************************!*\
  !*** external "reactstrap" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("reactstrap");

/***/ }),

/***/ "svg-inline-react":
/*!***********************************!*\
  !*** external "svg-inline-react" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("svg-inline-react");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map