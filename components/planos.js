import React from 'react';
import { Container, Row, Col, Table} from 'reactstrap';

const Planos = () => {

  return (
    <section className="box-planos pt-3 pt-md-2 pb-3 pb-md-4">
        <Container>
        <Row>
            <Col className="Col text-center text-primary mb-3 mb-md-5">
                <h2 className="pt-3 pt-md-5 mb-3 mb-md-5 title-vhsys"><span>Preços justos, sem adesão, sem surpresa</span></h2>
                <p>Por ser um sistema para empresas online, você não precisa pagar taxa de instalação e manutenção do software de gestão. Basta acessar o sistema com qualquer dispositivo que tenha acesso à internet. Você pode optar por um dos seis planos disponíveis, todos eles têm acesso total às funcionalidades e aplicativos do software, a variação se dá no número de usuários e emissões de notas fiscais eletrônicas. Veja qual é o melhor plano para as necessidades da sua empresa.</p>
            </Col>
        </Row>
        <div className="position-relative mt-md-4">
            <div className="box-table text-center">
                <Table className="table-responsive-xl">
                    <thead>
                    <tr>
                        <th>
                            <img src="/static/icon-plano-bronze.png" alt="MEI" className="mei" />  
                            <h2 className="font-weight-bold mb-0 mt-1">MEI</h2>      
                            <span className="old-price text-gray small">R$ 59,90</span>   
                            <span className="price"><small>por</small> 41 <span>,93</span></span>   
                            <span className="d-block text-gray small">por mês no plano anual <br /> Economize  <span className="text-primary font-weight-bold">R$ 215,64</span></span>          
                            <a className="btn btn-outline-primary rounded-pill text-uppercase">Contratar</a>
                        </th>
                        <th>
                            <img src="/static/icon-plano-bronze.png" alt="Bronze" />  
                            <h2 className="font-weight-bold mb-0 mt-1">Bronze</h2>      
                            <span className="old-price text-gray small">R$ 79,90</span>   
                            <span className="price"><small>por</small> 55 <span>,93</span></span>   
                            <span className="d-block text-gray small">por mês no plano anual <br /> Economize  <span className="text-primary font-weight-bold">R$ 284,64</span></span>          
                            <a className="btn btn-outline-primary rounded-pill text-uppercase">Contratar</a>
                        </th>
                        <th>
                            <img src="/static/icon-plano-prata.png" alt="Prata" />  
                            <h2 className="font-weight-bold mb-0 mt-1">Prata</h2>      
                            <span className="old-price text-gray small">R$ 99,90</span>   
                            <span className="price"><small>por</small> 69 <span>,93</span></span>   
                            <span className="d-block text-gray small">por mês no plano anual <br /> Economize  <span className="text-primary font-weight-bold">R$ 359,64</span></span>          
                            <a className="btn btn-outline-primary rounded-pill text-uppercase">Contratar</a>
                        </th>
                        <th>
                            <img src="/static/icon-plano-ouro.png" alt="Ouro" />  
                            <h2 className="font-weight-bold mb-0 mt-1">Ouro</h2>      
                            <span className="old-price text-white small">R$ 169,90</span>   
                            <span className="price"><small>por</small> 118 <span>,93</span></span>   
                            <span className="d-block text-white small">por mês no plano anual <br /> Economize  <span className="text-white font-weight-bold">R$ 611,64</span></span>          
                            <a className="btn btn-success rounded-pill text-uppercase">Contratar</a>
                        </th>
                        <th>
                            <img src="/static/icon-plano-platina.png" alt="Platina" />  
                            <h2 className="font-weight-bold mb-0 mt-1">Platina</h2>      
                            <span className="old-price text-gray small">R$ 259,90</span>   
                            <span className="price"><small>por</small> 181 <span>,93</span></span>   
                            <span className="d-block text-gray small">por mês no plano anual <br /> Economize  <span className="text-primary font-weight-bold">R$ 935,64</span></span>          
                            <a className="btn btn-outline-primary rounded-pill text-uppercase">Contratar</a>
                        </th>
                        <th>
                            <img src="/static/icon-plano-diamante.png" alt="Diamante" />  
                            <h2 className="font-weight-bold mb-0 mt-1">Diamante</h2>      
                            <span className="old-price text-gray small">R$ 299,90</span>   
                            <span className="price"><small>por</small> 209 <span>,93</span></span>   
                            <span className="d-block text-gray small">por mês no plano anual <br /> Economize  <span className="text-primary font-weight-bold">R$ 1.079,64</span></span>          
                            <a className="btn btn-outline-primary rounded-pill text-uppercase">Contratar</a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>25</td>
                        <td>60</td>
                        <td>150</td>
                        <td>300</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>25</td>
                        <td>125</td>
                        <td>300</td>
                        <td>750</td>
                        <td>1500</td>
                        <td>2500</td>                    
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>78</td>
                        <td>180</td>
                        <td>450</td>
                        <td>900</td>
                        <td>1500</td>
                    </tr>
                    <tr>
                        <td>Gratuito e ilimitado</td>
                        <td>Gratuito e ilimitado</td>
                        <td>Gratuito e ilimitado</td>
                        <td>Gratuito e ilimitado</td>
                        <td>Gratuito e ilimitado</td>
                        <td>Gratuito e ilimitado</td>
                    </tr>
                    </tbody>
                </Table>
            </div>
            <div className="box-table-legend">
                <span>Usuários </span>
                <span> NF-e e NFs-e </span>
                <span> NFC-e </span>
                <span> CT-e </span>
                <span>Suporte Técnico</span>
            </div>
        </div>

        </Container>
    </section>
  );
};

export default Planos;