import React from 'react';
import { Container, Row, Col } from 'reactstrap';


const Experimente = () => {
    return (
    <section className="box-experimente pt-2 pt-md-4 pb-3 pb-md-5 text-center">
        <Container>
            <Row>
                <Col className="Col text-center text-primary mb-3 mb-md-5">
                    <h4>Sistema de gestão completo para emitir notas fiscais, controlar o financeiro, gerenciar vendas e estoque! </h4>
                    <a href="#" className="btn bg-purple text-white pt-2 pb-2 pl-5 pr-5 mt-3 mt-5 rounded-pill">Experimente grátis por 7 dias</a>
                </Col>
            </Row>
            
        </Container>
    </section>
  );
};

export default Experimente;