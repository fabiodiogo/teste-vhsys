import React from 'react';
import { Container } from 'reactstrap';


const Experimente_bg = () => {

  return (
    <section className="box-experimente-bg text-white pt-3 pt-md-2 pb-3 pb-md-4">
        <Container>
            <div className="d-flex flex-md-row flex-column align-items-center">
                <div className="w-md-50">
                    <img src="/static/img-notebook.png" alt="notebook" />
                </div>
                <div className="w-md-50 text-center text-md-left">
                    <h2 className="mb-3 mb-md-3 h4">Faça como <strong>mais de 150 mil</strong> pequenas e médias empresas e experimente o Sistema de Gestão Online Gratuito por 7 dias!</h2>
                    <a href="#" className="btn bg-teal text-white pt-2 pb-2 pl-5 pr-5 mt-2 rounded-pill">Experimente grátis por 7 dias</a>
                </div>
            </div>
        </Container>
    </section>
  );
};

export default Experimente_bg;