import React from 'react';
import { Container} from 'reactstrap';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';

const Faq = () => {

  return (
    <section className="box-accordion pt-3 pt-md-2 pb-3 pb-md-4 mt-5">
        <Container>
        <h2 className="pt-md-5 mb-3 mb-md-5 mt-md-4 title-vhsys text-primary text-center">
            <span>
                Perguntas e respostas sobre <br /> Sistema de Gestão Online Gratuito
            </span>
        </h2>
        <Accordion>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        1. Quais as vantagens do sistema para clinica veterinária VHSYS?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p>Sistema de gestão é um software que te ajuda a gerenciar as atividades da sua empresa. Através de uma plataforma inteligente ele te permite controlar o financeiro, vendas ou prestação de serviços, estoque, clientes, produtos, contas bancárias. Te permite emitir nota fiscal eletrônica (nf-e), cupons fiscais, boletos, pedidos, orçamentos, ordens de serviços e muito mais. </p>
                    <p>Com um sistema de gestão é possível receber relatórios gerenciais, indicadores e gráficos e visualizar os resultados da empresa de forma simples e rápida. Isso te ajuda a ter uma visão estratégica e tomar decisões com mais facilidade.  </p>
                    <p>O sistema de gestão também centraliza as informações de todos os setores do negócio e, por isso, consegue automatizar tarefas. Com poucos cliques você pode, por exemplo, preencher uma nota fiscal eletrônica, fazer um orçamento, lançar contas no financeiro, baixar produtos do estoque, etc. </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        2. O sistema de gestã online é gratuito?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                <p>Sistema de gestão é um software que te ajuda a gerenciar as atividades da sua empresa. Através de uma plataforma inteligente ele te permite controlar o financeiro, vendas ou prestação de serviços, estoque, clientes, produtos, contas bancárias. Te permite emitir nota fiscal eletrônica (nf-e), cupons fiscais, boletos, pedidos, orçamentos, ordens de serviços e muito mais. </p>
                    <p>Com um sistema de gestão é possível receber relatórios gerenciais, indicadores e gráficos e visualizar os resultados da empresa de forma simples e rápida. Isso te ajuda a ter uma visão estratégica e tomar decisões com mais facilidade.  </p>
                    <p>O sistema de gestão também centraliza as informações de todos os setores do negócio e, por isso, consegue automatizar tarefas. Com poucos cliques você pode, por exemplo, preencher uma nota fiscal eletrônica, fazer um orçamento, lançar contas no financeiro, baixar produtos do estoque, etc. </p>
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        3. Quais são as funcionalidades do sistema de gestão online da VHSYS?
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                <p>Sistema de gestão é um software que te ajuda a gerenciar as atividades da sua empresa. Através de uma plataforma inteligente ele te permite controlar o financeiro, vendas ou prestação de serviços, estoque, clientes, produtos, contas bancárias. Te permite emitir nota fiscal eletrônica (nf-e), cupons fiscais, boletos, pedidos, orçamentos, ordens de serviços e muito mais. </p>
                    <p>Com um sistema de gestão é possível receber relatórios gerenciais, indicadores e gráficos e visualizar os resultados da empresa de forma simples e rápida. Isso te ajuda a ter uma visão estratégica e tomar decisões com mais facilidade.  </p>
                    <p>O sistema de gestão também centraliza as informações de todos os setores do negócio e, por isso, consegue automatizar tarefas. Com poucos cliques você pode, por exemplo, preencher uma nota fiscal eletrônica, fazer um orçamento, lançar contas no financeiro, baixar produtos do estoque, etc. </p>
                </AccordionItemPanel>
            </AccordionItem>
        </Accordion>

        </Container>
    </section>
  );
};

export default Faq;