import React from 'react';
import { Container, Row, Col , ListGroup, ListGroupItem ,  Button, Form, FormGroup, Label, Input} from 'reactstrap';

const Destaque = () => {
  return (
    <section className="box-destaque pt-3 pt-md-5 pb-3 pb-md-5">
        <Container>
            <Row>
                <Col lg="6" md="8" xs="12" className="text-white pt-3 pt-md-5">
                    <h1 className="mb-5">
                        Sistema de Gestão Online Gratuito
                        <span className="d-block"><span className="text-teal">Experimente Grátis</span> <strong>por 7 dias!</strong></span>
                    </h1>
                    <p>Sistema de gestão completo para pequenas e médias empresas</p>
                    <p>O VHSYS é o Sistema de Gestão Empresarial Online que integra todas as áreas da sua empresa. Com ele você têm:</p>
                    <Row className="mt-5 mb-5">
                        <Col md="7" xs="12">
                            <ListGroup>
                                <ListGroupItem>Emissão de notas fiscais eletrônicas;</ListGroupItem>
                                <ListGroupItem>Gestão de vendas e estoque;</ListGroupItem>
                                <ListGroupItem>Controle financeiro;</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <Col md="5" xs="12">
                            <ListGroup>
                                <ListGroupItem>Emissão de boletos;</ListGroupItem>
                                <ListGroupItem>Orçamento de vendas;</ListGroupItem>
                                <ListGroupItem>Integração contábil;</ListGroupItem>
                            </ListGroup>
                        </Col>   
                    </Row>
                </Col>  
                <Col lg={{ size: 4, offset: 2 }} xs="12" md="4" className="box-form">
                    <Form className="bg-white rounded shadow p-4">
                        <FormGroup>
                            <Label for="exampleEmail">Digite um e-mail</Label>
                            <Input type="email" name="user_email" id="user_email" placeholder="seu@email.com" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="examplePassword">Nome completo</Label>
                            <Input type="text" name="user_name" id="user_name" placeholder="Seu nome.." />
                        </FormGroup>
                        <FormGroup>
                            <Label for="examplePassword">Já tem uma empresa formalizada?</Label>
                            <Input type="select" name="user_company" id="user_company">
                                <option value="0">Sim</option>
                                <option value="1">Não</option>
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="examplePassword">Digite seu nome de usuário</Label>
                            <Input type="text" name="user_nick" id="user_nick" placeholder="usuario" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="examplePassword">Digite uma senha</Label>
                            <Input type="password" name="user_password" id="user_password" placeholder="senha" />
                        </FormGroup>
                        <Button className="rounded-pill w-100">Experimente Grátis</Button>
                        <small>Clicando no botão acima você concorda com nossos <a href="#">termos de uso.</a></small>
                    </Form>
                </Col>
            </Row>
        </Container>
    </section>
  );
};

export default Destaque;