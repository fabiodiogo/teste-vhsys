import React from 'react';
import Layout from '../components/Layout';
import Destaque from '../components/Destaque';
import Modulos from '../components/Modulos';
import Experimente from '../components/experimente';
import Solucoes from '../components/solucoes';
import Experimente_bg from '../components/experimente-bg';
import Planos from '../components/planos';
import Faq from '../components/faq';

import '../assets/css/App.css'; 

const Index = () => {
  return (
    <Layout>
      <Destaque />
      <Modulos />
      <Experimente />
      <Solucoes />
      <Experimente_bg />
      <Planos />
      <Faq />
    </Layout>
  );
};

export default Index;
